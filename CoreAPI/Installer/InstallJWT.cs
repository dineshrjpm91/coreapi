﻿using CoreAPI.Installer.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using CoreAPI.Providers;
using System.Text;
using System.Security.Claims;
using CoreAPI.Application.Repository;
using CoreAPI.Models;
using CoreAPI.Application.Contexts;
using MongoDB.Bson;
using System.Security.Principal;
using Microsoft.Extensions.Options;

namespace CoreAPI.Installer
{
    public class InstallJWT :IInstaller
    {

        private  SymmetricSecurityKey _signingKey;

        private  TokenValidationParameters _tokenValidationParameters;

        private  TokenProviderOptions _tokenProviderOptions;

        private IConfiguration config;


        public void ConfigServices(IApplicationBuilder app, IWebHostEnvironment env, IConfiguration configuration)
        {
           // app.UseStaticFiles();
           // app.UseSession();
          //  app.UseCookiePolicy();
            app.UseMiddleware<TokenProvider>(Options.Create(_tokenProviderOptions));
        }

        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {

            this.config = configuration;
            int timeout = 60;
            int.TryParse(configuration.GetSection("SessionStore:Timeout").Value, out timeout);

            String applicationBasePath = configuration.GetSection("Application:BasePath").Value;
            String basePathReadFromCommand = configuration.GetValue<string>("base_url");

            if (!String.IsNullOrEmpty(basePathReadFromCommand))
            {
                applicationBasePath = basePathReadFromCommand;
            }


            _signingKey =
              new SymmetricSecurityKey(
                  Encoding.ASCII.GetBytes(configuration.GetSection("TokenAuthentication:SecretKey").Value));

            _tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,
                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = configuration.GetSection("TokenAuthentication:Issuer").Value,
                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = configuration.GetSection("TokenAuthentication:Audience").Value,
                // Validate the token expiry
                ValidateLifetime = true,

                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };


            _tokenProviderOptions = new TokenProviderOptions
            {
                Login = applicationBasePath + "auth/login",
                Expiration = TimeSpan.FromMinutes(timeout),
                Path = "/" + configuration.GetSection("TokenAuthentication:TokenPath").Value,
                Audience = configuration.GetSection("TokenAuthentication:Audience").Value,
                Issuer = configuration.GetSection("TokenAuthentication:Issuer").Value,
                SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256),
                IdentityResolver = GetIdentity

            };

            services.AddMvc(option => option.EnableEndpointRouting = false).AddNewtonsoftJson();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
           .AddJwtBearer(options => { options.TokenValidationParameters = _tokenValidationParameters; })
           .AddCookie(options =>
           {
               options.Cookie.Name = configuration.GetSection("TokenAuthentication:CookieName").Value;
               options.TicketDataFormat = new TokenDataFormat(
                   SecurityAlgorithms.HmacSha256,
                   _tokenValidationParameters);
           });
        }

        private Task<ClaimsIdentity> GetIdentity(string _username, string _password)
        {
            var user = new Users()
            {
                id = _username,
                password = _password
            };
            var AuthRepo = new AuthRepository(new MongoContext(config));
            var authUser = AuthRepo.Login(user);
            if (authUser.Exception != null)
            {
                throw authUser.Exception;
            }
            if (user.id == authUser.Result.username)
            {
                var roles_details = AuthRepo.getUserRole(authUser.Result.user_role);
                if (roles_details != null)
                {
                    var claims = new Claim[]{
                        new Claim("user_type", authUser.Result.user_type.ToString()),
                        new Claim("user_role", authUser.Result.user_role.ToString()),
                        new Claim("user_email", authUser.Result.email.ToString()),
                        new Claim("user_role_details", roles_details.details.ToJson()),
                        new Claim("user_full_name", authUser.Result.full_name.getFullname()),
                        new Claim("client_id", authUser.Result.client_id.ToString())
                     };
                    return Task.FromResult(new ClaimsIdentity(new GenericIdentity(_username, "Token"), claims));
                }
                throw new System.Exception("Unknown role");
            }
            // Credentials are invalid, or account doesn't exist
            return Task.FromResult<ClaimsIdentity>(null);
        }
    }
}
