﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Installer.Interface
{
    public interface IInstaller
    {
       public void InstallServices(IServiceCollection services,IConfiguration configuration);

       public void ConfigServices(IApplicationBuilder app, IWebHostEnvironment env, IConfiguration configuration);
    }
}
