﻿using CoreAPI.Application.Operations;
using CoreAPI.Installer.Interface;
using CoreAPI.Providers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Installer
{
    public class InstallSwagger : IInstaller
    {
       

        public void InstallServices(IServiceCollection services,IConfiguration config)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "My Core API",
                    Version = "v1"
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement{
                    {
                        new OpenApiSecurityScheme{
                            Reference = new OpenApiReference{
                                Id = "Bearer", //The name of the previously defined security scheme.
                                Type = ReferenceType.SecurityScheme
                            }
                        },new List<string>()
                    }
                });

                c.OperationFilter<SwaggerOperation>();
            });

            services.AddSwaggerGenNewtonsoftSupport();
        }

        public void ConfigServices(IApplicationBuilder app, IWebHostEnvironment env, IConfiguration configuration)
        {
          

            String baseurl = configuration.GetValue<string>("base_url");
            if (string.IsNullOrEmpty(baseurl))
            {
                baseurl = "/";
            }

            app.UseMiddleware<SwaggerAuthorized>();

            app.UseSwagger(options =>
            {
                options.RouteTemplate = "documents/{documentName}/docs.json";
            });

            app.UseSwaggerUI(options =>
            {
                //  c.RoutePrefix = "documents";
                // c.SwaggerEndpoint("../documents/v1/docs.json", "My Core API V1");
                //c.SwaggerEndpoint(baseurl+"swagger/v1/swagger.json", "My Core API V1");

                options.RoutePrefix = "documents";
                options.SwaggerEndpoint("../documents/v1/docs.json", "My Core API V1");

                options.InjectJavascript(baseurl + "lib/jquery/dist/jquery.js", "text/javascript");
                options.InjectStylesheet(baseurl + "swagger/dist/custom.css", "text/css");
                options.InjectJavascript(baseurl + "swagger/dist/path-info.js", "text/javascript");
                options.InjectJavascript(baseurl + "swagger/dist/_custom.js", "text/javascript");
            });
        }


    }
}
