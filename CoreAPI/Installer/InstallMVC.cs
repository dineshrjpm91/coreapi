﻿using CoreAPI.Installer.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Installer
{
    public class InstallMVC 
    {
        public void ConfigServices(IApplicationBuilder app, IWebHostEnvironment env, IConfiguration configuration)
        {
           // app.UseMvc();
        }

        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMvc(option => option.EnableEndpointRouting = false).AddNewtonsoftJson();
        }
    }
}
