﻿using CoreAPI.Installer.Interface;
using CoreAPI.Common.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Installer
{
   
    public class InstallCors:IInstaller
    {
        public CorsInfo cors = new CorsInfo { Enable_Cors = false, Allowed_Hosts = new List<string>() };

        private string corspolicy = "AllowedOrigins";

        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            if (configuration.GetValue<bool>("enable_cors"))
            {
                cors.Enable_Cors = true;
            }

            if (cors.Enable_Cors)
            {
                string allowedHostFromConfig = configuration.GetSection("AllowedHosts").Value;
                if (String.IsNullOrEmpty(allowedHostFromConfig))
                {
                    allowedHostFromConfig = "";
                }

                string allowedHostFromArgs = "";

                allowedHostFromArgs = configuration.GetValue<string>("allowed_hosts");
                if (String.IsNullOrEmpty(allowedHostFromArgs))
                {
                    allowedHostFromArgs = "";
                }

                allowedHostFromArgs = allowedHostFromConfig + "," + allowedHostFromArgs;

                try
                {
                    foreach (string host in allowedHostFromArgs.Split(","))
                    {
                        string _host = host.Trim();
                        if (_host != string.Empty && cors.Allowed_Hosts.IndexOf(_host) == -1)
                        {
                            cors.Allowed_Hosts.Add(_host);
                        }

                    }

                }

                catch (Exception e) { }
                Console.WriteLine("Cors Enabled for :: " + String.Join(",", cors.Allowed_Hosts));
            }


            services.AddCors(options =>
            {
                if (cors.Enable_Cors)
                {
                    options.AddPolicy(corspolicy,
                     builder =>
                     {
                         builder.WithOrigins(cors.Allowed_Hosts.ToArray())
                                .AllowAnyHeader()
                                .AllowAnyMethod();
                     });
                }
                else
                {
                    options.AddPolicy("AllowedOrigins", builder => { builder = builder; });
                }

            });
        }

        public void ConfigServices(IApplicationBuilder app, IWebHostEnvironment env, IConfiguration configuration)
        {
            app.UseCors(corspolicy);
        }



    }
}
