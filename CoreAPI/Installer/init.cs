﻿using CoreAPI.Installer.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Installer
{
    public static class Init
    {
        static List<IInstaller> installers;
        public static void InstallServiesInAssemby(this IServiceCollection services,IConfiguration config)
        {
             installers = typeof(Startup).Assembly.ExportedTypes.Where(x =>
              typeof(IInstaller).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract).Select(Activator.CreateInstance).Cast<IInstaller>().ToList();

            installers.ForEach(install => install.InstallServices(services, config));
                
        }


        public static void ConfigServiesInAssemby(this IApplicationBuilder builder, IWebHostEnvironment env, IConfiguration config)
        {          

            installers.ForEach(install => install.ConfigServices(builder, env, config));

        }

    }
}
