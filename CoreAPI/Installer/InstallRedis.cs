﻿using CoreAPI.Installer.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Installer
{
    public class InstallRedis : IInstaller
    {
        public void ConfigServices(IApplicationBuilder app, IWebHostEnvironment env, IConfiguration configuration)
        {
           
        }

        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            try
            {
                bool flag;
                bool.TryParse(configuration.GetSection("SessionStore:Redis:enable").Value, out flag);
                if (flag)
                {
                    services.AddDistributedRedisCache(options =>
                    {
                        options.Configuration = configuration.GetSection("SessionStore:Redis:server").Value;
                        options.InstanceName = configuration.GetSection("SessionStore:Redis:dbname").Value;
                    });
                }
            }
            catch (Exception e)
            {

            }
        }
    }
}
