﻿using CoreAPI.Installer.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Installer
{
    public class InstallLogging : IInstaller
    {
        public void ConfigServices(IApplicationBuilder app, IWebHostEnvironment env, IConfiguration configuration)
        {
            
        }

        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddLogging(builder =>
            {
                builder.AddConfiguration(configuration.GetSection("Logging"))
                    .AddConsole()
                    .AddDebug();
            });
        }
    }
}
