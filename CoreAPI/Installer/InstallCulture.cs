﻿using CoreAPI.Installer.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Installer
{
    public class InstallCulture : IInstaller
    {
        public void ConfigServices(IApplicationBuilder app, IWebHostEnvironment env, IConfiguration configuration)
        {
            app.UseRequestLocalization();
        }

        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture(System.Globalization.CultureInfo.CurrentCulture.Name);
                options.SupportedCultures = new List<System.Globalization.CultureInfo> { new System.Globalization.CultureInfo(System.Globalization.CultureInfo.CurrentCulture.Name), new System.Globalization.CultureInfo(System.Globalization.CultureInfo.CurrentCulture.Name) };
            });
        }
    }
}
