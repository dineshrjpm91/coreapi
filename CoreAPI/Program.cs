using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {

            // CreateHostBuilder(args).Build().Run();
            BuildWebHost(args).Run();
        }

        /* IHost: The host is the component that hosts and runs your application and its services.
             This is a generalization of the previous IWebHost but fullfills basically the same task: 
             It starts configured hosted services and makes sure that your app is running and working.*/

        /* IHostBuilder: The host builder constructs the host and configures various services.
             This is the generalization of the previous IWebHostBuilder but also basically does 
             the same just for generic IHost. It configures the host before the application starts.*/

        /* public static IHostBuilder CreateHostBuilder(string[] args)=>

                  Host.CreateDefaultBuilder(args)
                 .ConfigureWebHostDefaults(webBuilder =>
                 {
                     webBuilder.UseStartup<Startup>();
                 });
             }
         }*/

        public static IWebHost BuildWebHost(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddCommandLine(args)
                .Build();
            var hostUrl = configuration["hosturl"];
            if (!string.IsNullOrEmpty(hostUrl))
            {
                return WebHost.CreateDefaultBuilder(args)
                .UseUrls(hostUrl)
                .UseConfiguration(configuration)
                .UseStartup<Startup>()
                .UseKestrel(options =>
                {
                    options.Limits.MaxRequestBodySize = int.MaxValue;
                    //options.Limits.MaxRequestBodySize = 2147482548; //2GB
                })
                .Build();
            }
            else
            {
                return WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(configuration)
                .UseStartup<Startup>()
                .UseKestrel(options =>
                {
                    options.Limits.MaxRequestBodySize = int.MaxValue;
                    //options.Limits.MaxRequestBodySize = 2147482548; //2GB
                })
                .Build();

            }

        }

    }
}
