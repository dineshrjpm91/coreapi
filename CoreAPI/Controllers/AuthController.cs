﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Caching.Memory;
using MongoDB.Bson;
using Newtonsoft.Json.Linq;
using CoreAPI.Common.Settings;
using CoreAPI.Application.Interfaces;
using CoreAPI.Application.Data;
using CoreAPI.Models;
using CoreAPI.Application.Utils;
using CoreAPI.Infrastructure;
using CoreAPI.Common.Memory;

namespace CoreAPI.Controllers
{
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    [EnableCors("AllowedOrigins")]
    public class AuthController : Controller
    {

        private readonly IOptions<ApplicationSettings> _settings;
        private readonly IMongoContext _context;
        private MemoryCache _cache;
        private readonly IWebHostEnvironment _env;
        string fileServerBasePath = Environment.GetEnvironmentVariable("PELLUCID_LOCAL_PATH");
        const String appFolderName = @"wwwroot/application/settings";
        readonly String folderPath;
        private readonly CRUDOperations _CRUD;
        private readonly AuditTrail _audit_logger;
        private readonly ITimezone _timeZone;
        public AuthController(ITimezone timeZone,ILoggerRepository loggerRepository, IOptionsSnapshot<ApplicationSettings> settings, IWebHostEnvironment env, IMongoContext context, MemCache memCache, IGenericRepository genericRepository, IUnitOfWork uow)
        {
            _timeZone = timeZone;
            if (fileServerBasePath != null)
            {
                //Path.Combine(Directory.GetCurrentDirectory(), folderName);

                folderPath = System.IO.Path.Combine(fileServerBasePath, appFolderName);
                IOOperations.checkAndCreateDirectory(folderPath);
            }
            else
            {
                throw new Exception("Configure missing for file server");
            }
            _settings = settings;
            _settings.Value.Init();
            _env = env;
            _context = context;
            _cache = memCache.Cache;
            _CRUD = new CRUDOperations(genericRepository, uow, context);
            _audit_logger = new AuditTrail(loggerRepository, uow, context);
        }


        [Authorize]
        [HttpGet("information")]
        [NoCache]
        public async Task<ActionResult<BsonDocument>> information()
        {
            string key = HttpContext.Session.GetString("JWToken");
            UserSession _user = new UserSession(this.User);
            string role_name = _user.user_role.Trim().ToLower();

            JObject j = new JObject();
            j["full_name"] = _user.full_name;
            j["id"] = _user.user_id;
            j["access_token"] = key;
            j["settings"] = new JObject();
            j["role_id"] = _user.user_role;
            j["type"] = _user.user_type;
            j["client_id"] = _user.client_id;

            /*
            if (_user.user_type == "1" || _user.user_type == "0")
            {
                JObject defaultApplicationSettings;

                if (_user.client_id == "pellucid")
                {
                    defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
                }
                else
                {
                    if (System.IO.File.Exists(_env.ContentRootPath + "//apps//" + "client_minimum_info.json"))
                    {
                        JObject clients = JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "client_minimum_info.json"));
                        try
                        {
                            defaultApplicationSettings =(JObject) Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(clients[_user.client_id]["settings"] + "app-settings.json"))["settings"];

                        }
                        catch (Exception e)
                        {
                            defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
                        }
                    }
                    else
                    {
                        defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
                    }

                }
                j["settings"] = defaultApplicationSettings;
            }
            else
            {
                j["settings"] = JObject.Parse(new UserRole(_context, _cache).getRoleDetails(role_name));
            }
            */


            JObject defaultApplicationSettings = new JObject();

            if (_user.client_id == "pellucid")
            {
                defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
            }
            else
            {
                if (System.IO.File.Exists(_env.ContentRootPath + "//apps//" + "client_minimum_info.json"))
                {
                    JObject clients = JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "client_minimum_info.json"));
                    try
                    {
                        defaultApplicationSettings = (JObject)Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(clients[_user.client_id]["settings"] + "app-settings.json"))["settings"];

                    }
                    catch (Exception e)
                    {
                        defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
                    }
                }
                else
                {
                    defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
                }
            }
            j["settings"] = defaultApplicationSettings;
            j["datetime"] = _timeZone.GetUserLocalTimeasString();
            j["roles"] = JObject.Parse(new UserRole(_context, _cache).getRoleDetails(role_name));
            return await Task.FromResult(Json(j));


        }



        [AllowAnonymous]
        [HttpGet("login")]
        [NoCache]
        public async Task<IActionResult> login()
        {
            Pages page = new Pages()
            {
                user = new Users(),
                settings = _settings.Value
            };
            var currentUserName = "unknown";
            try
            {
                UserSession u = new UserSession(this.User);

                currentUserName = u.full_name;

            }

            catch (Exception e) { e = e; }
            
            return await Task.FromResult(View("~/Views/Auth/Login.cshtml", page));
        }

        [AllowAnonymous]
        [HttpGet("logout")]
        [NoCache]
        public IActionResult logout()
        {
            
            try
            {
                UserSession u = new UserSession(this.User);
                _audit_logger.InsertLog(u.client_id + "__" + DateTime.Now.ToString("yyyyMM"), u.client_id, "Authorisation", "LOGOUT", u.user_id, _settings.Value.getSystemModel("pellucid_logs"), new JObject(), u, this.Request.HttpContext);
            }
            catch (Exception e) { }
            HttpContext.Session.Clear();
            return Redirect("~/auth/login");
        }
    }
}
