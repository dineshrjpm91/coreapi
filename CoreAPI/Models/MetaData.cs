﻿using System;
using Newtonsoft.Json.Linq;
namespace CoreAPI.Models
{
    public class MetaData
    {
        public string id;
        public string name;
        public string description;
        public JObject information;
        public JObject sub_modules;
        public DateTime? created;
        public DateTime? updated;
        public MetaData(string id,string name,string description,JObject information, JObject sub_modules)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.information = information;
            this.sub_modules = sub_modules;

        }
    }
}
