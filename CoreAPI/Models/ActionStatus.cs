﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreAPI.Common.Settings;

namespace CoreAPI.Models
{
    public class ActionStatus
    {
        public string id;
        public string system_id;
        public string status;
        public string update_on;
        public string unique_key;
        public ValidationErrors errors;
        public ActionStatus(string id,string status,string update_on,string system_id,string unique_key)
        {
            init(id, status, update_on, system_id, unique_key,new ValidationErrors());

        }
        public ActionStatus(string id, string status, string update_on, string system_id,string unique_key, ValidationErrors errors)
        {
            init( id,  status,  update_on,  system_id, unique_key, errors);
        }
        void init(string id, string status, string update_on, string system_id, string unique_key, ValidationErrors errors)
        {
            this.id = id;
            this.status = status;
            this.system_id = system_id;
            this.update_on = update_on;
            this.unique_key = unique_key;
            this.errors = errors;
        }
    }
}
