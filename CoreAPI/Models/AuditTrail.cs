﻿using CoreAPI.Application.Data;
using CoreAPI.Application.Interfaces;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Models
{
    public class AuditTrail
    {
        private readonly LoggerOperations _log;

        public AuditTrail(ILoggerRepository loggerRepository, IUnitOfWork uow, IMongoContext context)
        {
            _log = new LoggerOperations(loggerRepository, uow, context);
        }
        public async Task<object> InsertLogAsync(string collection_name, string client, string model_name, string event_type, string identity, JToken meta_data, JToken item, UserSession user, HttpContext request)
        {

            var log = new { id = Guid.NewGuid(), identity = identity, model_name = model_name, event_type = event_type, client = client, content = item, request = CoreAPI.Application.Utils.RequestHandling.formatRequest(request), user = new { user_id = user.user_id, full_name = user.full_name, email = user.email } };
            return await _log.Create(collection_name, meta_data, JObject.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(log)));

        }
        public object InsertLog(string collection_name, string client, string model_name, string event_type, string identity, JToken meta_data, JToken item, UserSession user, HttpContext request)
        {
            var log = new { id = Guid.NewGuid(), identity = identity, model_name = model_name, event_type = event_type, client = client, content = item, request = CoreAPI.Application.Utils.RequestHandling.formatRequest(request), user = new { user_id = user.user_id, full_name = user.full_name, email = user.email } };
            return _log.Create(collection_name, meta_data, JObject.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(log))).Result;
        }

        public async Task<object> InsertLogAsync(string collection_name, string client, string model_name, string event_type, string identity, JToken meta_data, JToken item, Object user, HttpContext request)
        {

            var log = new { id = Guid.NewGuid(), identity = identity, model_name = model_name, event_type = event_type, client = client, content = item, request = CoreAPI.Application.Utils.RequestHandling.formatRequest(request), user = user };
            return await _log.Create(collection_name, meta_data, JObject.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(log)));

        }
        public object InsertLog(string collection_name, string client, string model_name, string event_type, string identity, JToken meta_data, JToken item, Object user, HttpContext request)
        {
            var log = new { id = Guid.NewGuid(), identity = identity, model_name = model_name, event_type = event_type, client = client, content = item, request = CoreAPI.Application.Utils.RequestHandling.formatRequest(request), user = user };
            return _log.Create(collection_name, meta_data, JObject.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(log))).Result;
        }
    }
}
