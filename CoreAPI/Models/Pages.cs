﻿using CoreAPI.Common.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Models
{
    public class Pages
    {
        public Users user { get; set; }
        public ApplicationSettings settings { get; set; }

        public PrintOptions print { get; set; }
       
    }


}
