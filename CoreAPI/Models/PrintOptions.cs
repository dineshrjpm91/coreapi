﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Models
{  
    public class PrintOptions
    {
        public string page_size { get; set; }
        public string page_header { get; set; }   
        public string page_title { get; set; }

        public string logo { get; set; }
        public string page_sub_title { get; set; } 
        public string page_styles { get; set; } 
        public string page_content { get; set; }
        public string page_footer { get; set; }
        public string filename { get; set; }

        public string display_header { get; set; }
        public string display_footer { get; set; }
        public string page_header_size { get; set; }
        public string page_footer_size { get; set; }
        
        public string page_orientation { get; set; }
        
    }

}
