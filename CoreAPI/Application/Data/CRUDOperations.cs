﻿using System;
using System.Threading;
using System.Net.Mime;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.IO;
using Newtonsoft.Json.Linq;
using CoreAPI.Application.Interfaces;
using CoreAPI.Models;
using CoreAPI.Common.Settings;

namespace CoreAPI.Application.Data
{
    public class CRUDOperations
    {
        private IGenericRepository _genericRepository;
        private IUnitOfWork _uow;
        private IMongoContext _context;
        private readonly SemaphoreSlim autoGenerationLock = new SemaphoreSlim(1, 1);
        public CRUDOperations(IGenericRepository genericRepository, IUnitOfWork uow, IMongoContext context)
        {
            _genericRepository = genericRepository;
            _uow = uow;
            _context = context;
        }
        public async Task<BsonDocument> Get_By_Unique_Constraints(string model, JToken model_info, JObject Item)
        {
            string version = "0.0.0";

            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim(); ;
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;
            JArray constraints_from_module_info = new JArray();
            constraints_from_module_info = (JArray)model_info?["modules_information"]?["unique_keys_contraints"];
            JObject item_info = new JObject();
            return await _genericRepository.Get_Item_By_Unique_Constraints(Utils.Util<BsonDocument>.formalateUniqueConstraint(_id_col, Item, model.Trim().ToLower(), constraints_from_module_info, item_info));
        }
        public async Task<BsonDocument> Get(string model, JToken model_info, string limit, string page, string sort, string sortBy, string query, string exclude)
        {
            JObject QUERY = null;
            try
            {
                QUERY = JObject.Parse(query);
            }
            catch (Exception e)
            {
                QUERY = new JObject();
            }
            limit = limit ?? "100";
            sort = sort ?? "desc";
            sortBy = sortBy ?? "updated";
            exclude = exclude ?? "";
            page = page ?? "1";
            int _page = 1;
            string _sort = "asc";
            int _limit = 100;
            try
            {
                _limit = int.Parse(limit);
                if (_limit > 1000)
                {
                    _limit = 100;
                }
            }
            catch (Exception e)
            {
            }
            try
            {
                _page = int.Parse(page);
                if (_page < 0)
                {
                    _page = 1;
                }

            }
            catch (Exception e)
            {
            }
            try
            {
                _sort = sort.Trim().ToLower() == "asc" ? "asc" : "desc";

            }
            catch (Exception e)
            {
            }

            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim(); ;
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;


            BsonDocument document = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model.Trim().ToLower() },
                                {"IDENTITY",_id_col }
                            }
                        },
                        { "PARAMS", new BsonDocument{
                            {"SORTING" , new BsonDocument
                            {
                                { "SORT" ,_sort },
                                {"SORT_BY", sortBy.Trim().ToLower()}

                            }},
                            { "QUERY" , QUERY.ToString()},
                            {"EXCLUDE" , new BsonDocument {
                                { "FILEDS", exclude }
                            } }


                        } },
                        { "PAGER" , new BsonDocument{
                            { "LIMIT" , _limit },
                            { "PAGE" , --_page }
                        } }
                    };
            return await _genericRepository.GetAllData(document);

        }
        public async Task<BsonDocument> GetDetails(string model, JToken model_info)
        {
            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim(); ;
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;


            BsonDocument document = new BsonDocument
                {
                    { "META_DATA", new BsonDocument{
                            { "ID",model.Trim().ToLower() },
                            {"IDENTITY",_id_col}
                        }
                    },

                };
            return await _genericRepository.GetDetails(document);
        }
        public async Task<BsonDocument> GetModelByID(string model, JToken model_info, string id)
        {
            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim(); ;
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;


            BsonDocument document = new BsonDocument
                {
                    { "META_DATA", new BsonDocument{
                            { "ID",model.Trim().ToLower() },
                            {"IDENTITY",_id_col}
                        }
                    },
                    { "VALUE" , new BsonDocument{
                        { "identity_value" ,  id.ToString() },
                    } }
                };
            var content = await _genericRepository.GetById(document);
            if (content != null && content[_id_col].ToString().ToLower() != id.ToLower())
            {
                content = null;
            }
            return content;
        }
        public async Task<BsonDocument> PartialUpdate(string model, string id, JToken model_info, JObject Item)
        {
            string version = "0.0.0";

            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim(); ;
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;

            try
            {
                Item[_id_col] = id;
                Item["version"] = version;
            }
            catch (Exception e)
            {

            }

            version = model_info["version"] != null ? model_info["version"].ToString() : version;

            //get the content 

            BsonDocument get_document = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model.Trim().ToLower() },
                                { "VERSION",version },
                                {"IDENTITY",_id_col }
                            }
                        },
                        { "VALUE" , BsonDocument.Parse(Item.ToString())}
                    };

            var get_content = await _genericRepository.GetById(get_document);

            if (get_content == null)
            {
                return null;
            }
            else
            {
                JObject contentFromDB = JObject.Parse(get_content.ToString());
                contentFromDB.Merge(Item, new JsonMergeSettings
                {
                    // union array values together to avoid duplicates
                    MergeArrayHandling = MergeArrayHandling.Replace,
                    MergeNullValueHandling = MergeNullValueHandling.Merge

                });
                Item = contentFromDB;
            }

            //Item = (JObject)Utils.Util<JObject>.RemoveEmptyChildren(Item);
            var validate = JSONValidator.validateSchema(model_info["meta_data"].ToString(), Item);

            if (validate.errors.Count > 0)
            {
                var result = new BsonDocument
                        {
                            {"CODE",500 },
                            { "EXCEPTION", true},
                            { "ERRORS",  validate.ToBsonDocument()}
                        };
                return await Task.FromResult(result);
            }
            else
            {
                try
                {
                    Item[_id_col] = id;
                    Item["version"] = version;
                }
                catch (Exception e)
                {

                }
                BsonDocument document = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model.Trim().ToLower() },
                                { "VERSION",version },
                                {"IDENTITY",_id_col }
                            }
                        },
                        { "VALUE" , BsonDocument.Parse(Item.ToString())}
                    };

                _genericRepository.Update(document);

                await _uow.Commit();
                return await _genericRepository.GetById(document);
            }
        }
        public async Task<BsonDocument> Replace(string model, string id, JToken model_info, JObject Item)
        {
            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim(); ;
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;


            string version = "0.0.0";
            version = model_info["version"] != null ? model_info["version"].ToString() : version;

            var validate = JSONValidator.validateSchema(model_info["meta_data"].ToString(), Item);

            if (validate.errors.Count > 0)
            {
                var result = new BsonDocument
                        {
                            {"CODE",500 },
                            { "EXCEPTION", true},
                            { "ERRORS",  validate.ToBsonDocument()}
                        };
                return await Task.FromResult(result);
            }
            else
            {
                try
                {
                    Item[_id_col] = id;

                    Item[_id_col] = Utils.IDFormator.ReplaceInvalidChars(Item[_id_col].ToString());

                    Item["version"] = version;



                }
                catch (Exception e)
                {

                }


                //Check for the item 

                BsonDocument uniqueConstraintCheck = new BsonDocument();
                JArray constraints_from_module_info = new JArray();
                constraints_from_module_info.Add(_id_col);

                if (constraints_from_module_info != null && (constraints_from_module_info.Count > 0))
                {
                    JObject item_info = new JObject();
                    item_info["ID"] = _id_col;
                    item_info["VALUE"] = id;
                    uniqueConstraintCheck = _genericRepository.Check_For_Unique_Constraints(Utils.Util<BsonDocument>.formalateUniqueConstraint(_id_col, Item, model.Trim().ToLower(), constraints_from_module_info, item_info)).Result;
                    //raise conflict error if posting exiting item 
                    if (uniqueConstraintCheck["SAME_ITEM"].ToBoolean())
                    {

                        constraints_from_module_info = (JArray)model_info?["modules_information"]?["unique_keys_contraints"];

                        if (constraints_from_module_info != null && (constraints_from_module_info.Count > 0))
                        {
                            //raise conflict error if posting exiting item with the constraint
                            var _contraints_for_existing = _genericRepository.Check_For_Unique_Constraints(Utils.Util<BsonDocument>.formalateUniqueConstraint(_id_col, Item, model.Trim().ToLower(), constraints_from_module_info, item_info));
                            if (_contraints_for_existing.Result["EXIST"].ToBoolean() && !_contraints_for_existing.Result["SAME_ITEM"].ToBoolean())
                            {
                                return await _contraints_for_existing;
                            }

                        }


                        BsonDocument document = new BsonDocument
                            {
                                { "META_DATA", new BsonDocument{
                                        { "ID",model.Trim().ToLower() },
                                        { "VERSION",version },
                                        {"IDENTITY",_id_col}
                                    }
                                },
                                { "VALUE" , BsonDocument.Parse(Item.ToString())}
                            };

                        _genericRepository.Replace(document);

                        await _uow.Commit();
                        return await _genericRepository.GetById(document);

                    }
                    else
                    {
                        return null;
                    }

                }
                return null;
            }
        }
        public async Task<BsonDocument> Update(string model, string id, JToken model_info, JObject Item)
        {

            string version = "0.0.0";

            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim(); ;
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;


            version = model_info["version"] != null ? model_info["version"].ToString() : version;

            var validate = JSONValidator.validateSchema(model_info["meta_data"].ToString(), Item);

            if (validate.errors.Count > 0)
            {
                var result = new BsonDocument
                        {
                            {"CODE",500 },
                            { "EXCEPTION", true},
                            { "ERRORS",  validate.ToBsonDocument()}
                        };
                return await Task.FromResult(result);
            }
            else
            {
                try
                {
                    Item[_id_col] = id;

                    Item[_id_col] = Utils.IDFormator.ReplaceInvalidChars(Item[_id_col].ToString());

                    Item["version"] = version;
                }
                catch (Exception e)
                {

                }
                // check for item exist
                BsonDocument uniqueConstraintCheck = new BsonDocument();
                JArray constraints_from_module_info = new JArray();
                constraints_from_module_info.Add(_id_col);

                if (constraints_from_module_info != null && (constraints_from_module_info.Count > 0))
                {
                    JObject item_info = new JObject();
                    item_info["ID"] = _id_col;
                    item_info["VALUE"] = id;
                    uniqueConstraintCheck = _genericRepository.Check_For_Unique_Constraints(Utils.Util<BsonDocument>.formalateUniqueConstraint(_id_col, Item, model.Trim().ToLower(), constraints_from_module_info, item_info)).Result;
                    // check for same item
                    if (uniqueConstraintCheck["SAME_ITEM"].ToBoolean())
                    {

                        constraints_from_module_info = (JArray)model_info?["modules_information"]?["unique_keys_contraints"];

                        if (constraints_from_module_info != null && (constraints_from_module_info.Count > 0))
                        {
                            //raise conflict error if posting exiting item with the constraint
                            var _contraints_for_existing = _genericRepository.Check_For_Unique_Constraints(Utils.Util<BsonDocument>.formalateUniqueConstraint(_id_col, Item, model.Trim().ToLower(), constraints_from_module_info, item_info));
                            if (_contraints_for_existing.Result["EXIST"].ToBoolean() && !_contraints_for_existing.Result["SAME_ITEM"].ToBoolean())
                            {
                                return await _contraints_for_existing;
                            }
                        }


                        BsonDocument document = new BsonDocument
                            {
                                { "META_DATA", new BsonDocument{
                                        { "ID",model.Trim().ToLower() },
                                        { "VERSION",version },
                                        {"IDENTITY",_id_col}
                                    }
                                },
                                { "VALUE" , BsonDocument.Parse(Item.ToString())}
                            };

                        _genericRepository.Update(document);

                        await _uow.Commit();
                        return await _genericRepository.GetById(document);
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }

        }
        public async Task<BsonDocument> Create(string model, JToken model_info, JObject Item)
        {

            string version = "0.0.0";

            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim();
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;

           

            version = model_info["version"] != null ? model_info["version"].ToString() : version;


            #region Auto generate ID

            string auto_gen_id = model_info?["modules_information"]?["auto_increment"]?["field_name"].ToString().ToLower().Trim();

            if (auto_gen_id != null)
            {
                Item[auto_gen_id] = Guid.NewGuid().ToString();

            }

            #endregion



            var validate = JSONValidator.validateSchema(model_info["meta_data"].ToString(), Item);

            if (validate.errors.Count > 0)
            {
                var result = new BsonDocument
                        {
                            {"CODE",500 },
                            { "EXCEPTION", true},
                            { "ERRORS",  validate.ToBsonDocument()}
                        };
                //var result = new BadRequestObjectResult(validate);
                //result.ContentTypes.Add(MediaTypeNames.Application.Json);
                //return result;
                return await Task.FromResult(result);
            }
            else
            {
                // auto generate ID 
                BsonDocument uniqueConstraintCheck = new BsonDocument();

                JArray constraints_from_module_info = (JArray)model_info?["modules_information"]?["unique_keys_contraints"];
                if (constraints_from_module_info != null && (constraints_from_module_info.Count > 0))
                {
                    //raise conflict error if posting exiting item 
                    //trim values to avoid duplicate
                    try
                    {
                        Item = Utils.Util<BsonDocument>.formatItem(_id_col, Item, model.Trim().ToLower(), constraints_from_module_info, null);
                    }
                    catch(Exception e) { }
                    var _contraints = _genericRepository.Check_For_Unique_Constraints(Utils.Util<BsonDocument>.formalateUniqueConstraint(_id_col, Item, model.Trim().ToLower(), constraints_from_module_info, null));
                    if (_contraints.Result["EXIST"].ToBoolean())
                    {
                        return await _contraints;
                    }

                }

                if (auto_gen_id != null)
                {
                    string query_model_name = "client_models_counter";
                    if (model.Contains("__"))
                    {
                        string[] info = model.Split("__");
                        if (info.Length >0)
                        {
                            query_model_name = info[0] + "__" + "client_models_counter";
                        }
                    }
                    BsonDocument auto_generate = new BsonDocument
                        {
                            { "META_DATA", new BsonDocument{
                                    { "ID",query_model_name },
                                 }
                            },
                            { "PARAMS", new BsonDocument{
                                { "INFORMATION" , BsonDocument.Parse(model_info.ToString()) }}
                            }
                        };
                    await autoGenerationLock.WaitAsync();
                    try
                    {
                        Item[auto_gen_id] = Utils.Util<string>.FormatAutoGeneratedID((JObject)model_info?["modules_information"]?["auto_increment"], long.Parse(_genericRepository.AUTO_Generate_ID(auto_generate).Result["sequence"].ToString()));
                    }
                    finally
                    {
                        autoGenerationLock.Release();
                    }
                }

                //look for id already exist

                Item[_id_col] = Item[_id_col].ToString().Trim();
                Item[_id_col] = Utils.IDFormator.ReplaceInvalidChars(Item[_id_col].ToString());
                constraints_from_module_info = new JArray();
                constraints_from_module_info.Add(_id_col);
                if (constraints_from_module_info != null && (constraints_from_module_info.Count > 0))
                {
                    constraints_from_module_info = new JArray();
                    //raise conflict error if posting exiting item 
                    var _contraints = _genericRepository.Check_For_Unique_Constraints(Utils.Util<BsonDocument>.formalateUniqueConstraint(_id_col, Item, model.Trim().ToLower(), constraints_from_module_info, null));
                    if (_contraints.Result["EXIST"].ToBoolean())
                    {
                        return await _contraints;
                    }
                }



                BsonDocument document = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model.Trim().ToLower() },
                                { "VERSION",version },
                                {"IDENTITY",_id_col }
                            }
                        },
                        { "VALUE" , BsonDocument.Parse(Item.ToString()) }
                    };

                _genericRepository.Add(document);


                // If everything is ok then:
                await _uow.Commit();

                document = new BsonDocument
                {
                    { "META_DATA", new BsonDocument{
                            { "ID",model.Trim().ToLower() },
                            {"IDENTITY",_id_col}
                        }
                    },
                    { "VALUE" , new BsonDocument{
                        { "identity_value" ,  Item[_id_col].ToString() },
                    } }
                };
                // The product will be added only after commit
                return await Task.FromResult(_genericRepository.GetById(document).Result);
                //return await _genericRepository.GetById(document);


            }
        }

        public async Task<BsonDocument> Delete(string model, string id, JToken model_info)
        {
            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim(); ;
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;


            BsonDocument document = new BsonDocument
                {
                    { "META_DATA", new BsonDocument{
                            { "ID",model.Trim().ToLower() },
                            {"IDENTITY",_id_col}
                        }
                    },
                    { "VALUE" , new BsonDocument{
                        { "identity_value" ,  id.ToString() },
                    } }
                };
            _genericRepository.Remove(document);

            // If everything is ok then:
            await _uow.Commit();

            // not it must by null
            return await _genericRepository.GetById(document);
        }
        public async Task<BsonDocument> BulkImport(string model, JToken model_info,JObject Information)
        {
            string version = "0.0.0";
            version = model_info["version"] != null ? model_info["version"].ToString() : version;


            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim(); ;
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;

            #region Auto generate ID

            string auto_gen_id = model_info?["modules_information"]?["auto_increment"]?["field_name"].ToString().ToLower().Trim();


            #endregion

            IDictionary<string, ActionStatus> ItemsStatus = new Dictionary<string, ActionStatus>();
            BsonArray lookForItems = new BsonArray();
            BsonArray items_for_add_or_update = new BsonArray();
            BsonArray errorItems = new BsonArray();

            NJsonSchema.JsonSchema schema = JSONValidator.loadSchema(model_info["meta_data"].ToString());

            // validate the input before posting 
            #region FORMAT INPUT DATA

            JArray constraints_from_module_info = (JArray)model_info?["modules_information"]?["unique_keys_contraints"];

            IDictionary<string, JObject> Item_constraints = new Dictionary<string, JObject>();

            IDictionary<string, JArray> Item_constraints_key_values = new Dictionary<string, JArray>();
            if (constraints_from_module_info != null && (constraints_from_module_info.Count > 0))
            {
                foreach (var contraints in constraints_from_module_info)
                {
                    string __id = contraints.ToString().Trim().ToLower();
                    if (!Item_constraints_key_values.ContainsKey(__id))
                    {
                        Item_constraints_key_values.Add(__id, new JArray());
                    }

                }
            }
            foreach (JObject item in Information["items"])
            {
                

                try
                {


                    bool isTempID = false;
                    if (auto_gen_id != null)
                    {
                        if (item[auto_gen_id] == null)
                        {
                            item[auto_gen_id] = Guid.NewGuid().ToString();
                            isTempID = true;
                        }
                    }

                    item[_id_col] = Utils.IDFormator.ReplaceInvalidChars(item[_id_col].ToString());

                    // Validate schemas



                    string id = item[_id_col].ToString().Trim();
                    
                    var validate = JSONValidator.validateSchema(schema, item);
                    if (validate.errors.Count > 0)
                    {
                        if (!ItemsStatus.ContainsKey(id))
                        {
                            ItemsStatus.Add(id, new ActionStatus(id, "error", "", "", "", validate));
                        }
                    }
                    else
                    {
                        // format the constraints 
                        JObject temp = new JObject();
                        temp["id"] = id.Trim().ToLower();
                        temp["is_temp"] = isTempID;

                        if (constraints_from_module_info != null && (constraints_from_module_info.Count > 0))
                        {
                            string key = Utils.Util<string>.getConstraintValueForItem(item, constraints_from_module_info);
                            try
                            {
                                Item_constraints.Add(key, temp);
                                ItemsStatus.Add(id, new ActionStatus(id, "validate_constraints", "", "", key));
                            }
                            catch (Exception e)
                            {
                                string tempId =  id + "__" + Guid.NewGuid().ToString();
                                item[_id_col] = tempId;
                                ValidationErrors duplication_error = new ValidationErrors();
                                duplication_error.errors.Add("Duplicate item in import based on unique constraints");
                                ItemsStatus.Add(tempId, new ActionStatus(tempId, "error", "", "", key, duplication_error));
                            }
                        }
                        else
                        {
                            // add the other items if the constaints is empty or null
                            string key = temp["id"].ToString();
                            try
                            {
                                Item_constraints.Add(key, temp);
                                ItemsStatus.Add(id, new ActionStatus(id, "validate_constraints", "", "", key));
                            }
                            catch (Exception e)
                            {
                                string tempId = id + "__" + Guid.NewGuid().ToString();
                                item[_id_col] = tempId;
                                ValidationErrors duplication_error = new ValidationErrors();
                                duplication_error.errors.Add("Duplicate item in import");
                                ItemsStatus.Add(tempId, new ActionStatus(tempId, "error", "", "", key, duplication_error));
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    string id = item[_id_col].ToString().Trim();
                    string tempId = id + "__" + Guid.NewGuid().ToString();
                    item[_id_col] = tempId;
                    ValidationErrors duplication_error = new ValidationErrors();
                    duplication_error.errors.Add(e.Message);
                    ItemsStatus.Add(tempId, new ActionStatus(tempId, "error", "", "", "", duplication_error));
                }
            }
            //check with exiting object
            foreach (JObject item in Information["items"])
            {
                if (ItemsStatus[item[_id_col].ToString()].status.ToLower() == "validate_constraints")
                {
                    var is_new_item = Item_constraints.FirstOrDefault(dict => dict.Value.ContainsKey("id") && dict.Value["id"].ToString().Trim().ToLower() == item[_id_col].ToString().Trim().ToLower());
                    if (is_new_item.Value != null && is_new_item.Value["is_temp"].ToString().ToLower() == "true")
                    {
                    }
                    else
                    {
                        ItemsStatus[item[_id_col].ToString()].status = "check_for_existing";
                        lookForItems.Add(new BsonDocument{
                            { _id_col,item[_id_col].ToString().Trim() } });

                    }
                }
            }
            #endregion
            #region CHECK for EXISTING VALUE FROM DB

            if (lookForItems.Count > 0)
            {

                BsonDocument loolUpExistingItems = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model.Trim().ToLower() },
                                { "VERSION",version },
                                {"IDENTITY",_id_col}
                            }
                        },
                        { "VALUE" , lookForItems }
                    };
                var look_up_content = await _genericRepository.GetByIds(loolUpExistingItems, true);

                foreach (BsonDocument item in look_up_content)
                {
                    ActionStatus s = ItemsStatus[item[_id_col].ToString()];
                    s.status = "exist";
                    s.system_id = item["_id"].ToString();
                    s.update_on = item["updated"].ToString();
                    ItemsStatus[item[_id_col].ToString()] = s;
                }
                foreach (KeyValuePair<string, ActionStatus> item in ItemsStatus)
                {
                    if (item.Value.status == "check_for_existing")
                    {
                        item.Value.status = "look_for_constraints";
                    }
                }
            }
            #endregion

            #region PREPARE for validating CONSTRAINTS
            foreach (JObject item in Information["items"])
            {
                if (ItemsStatus[item[_id_col].ToString()].status.ToLower() == "validate_constraints")
                {
                    var is_new_item = Item_constraints.FirstOrDefault(dict => dict.Value.ContainsKey("id") && dict.Value["id"].ToString().Trim().ToLower() == item[_id_col].ToString().Trim().ToLower());
                    if (is_new_item.Value != null && is_new_item.Value["is_temp"].ToString().ToLower() == "true")
                    {
                        ItemsStatus[item[_id_col].ToString()].status = "look_for_constraints";
                        if (constraints_from_module_info != null && (constraints_from_module_info.Count > 0))
                        {
                            foreach (var contraints in constraints_from_module_info)
                            {
                                string __id = contraints.ToString().Trim().ToLower();

                                if (Item_constraints_key_values.ContainsKey(__id))
                                {
                                    var key_value = (string)item.SelectToken(__id);
                                    if (key_value != null)
                                    {
                                        key_value = key_value.ToString().Trim().ToLower();
                                        if (!Item_constraints_key_values[__id].Contains(key_value))
                                        {
                                            Item_constraints_key_values[__id].Add(key_value);
                                        }
                                    }
                                }


                            }
                        }
                    }

                }
            }
            #endregion
            #region Check for any contraints for new data



            bool validate_new_item_for_constraints = false;
            var QUERY = new JObject();
            foreach (KeyValuePair<string, JArray> item in Item_constraints_key_values)
            {
                if (item.Value.Count > 0)
                {
                    validate_new_item_for_constraints = true;
                    JArray query_items = new JArray();
                    var tempQuery = new JObject();
                    foreach (var q_item in item.Value)
                    {
                        tempQuery = new JObject();
                        tempQuery["$regex"] = "^" + q_item.ToString() + "$";
                        tempQuery["$options"] = "i";
                        query_items.Add(tempQuery);
                    }
                    tempQuery = new JObject();
                    tempQuery["$in"] = query_items;
                    QUERY[item.Key] = tempQuery;
                }
            }

            if (validate_new_item_for_constraints)
            {

                BsonDocument document = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model.Trim().ToLower() },
                                {"IDENTITY",_id_col }
                            }
                        },
                        { "PARAMS", new BsonDocument{
                            { "QUERY" , QUERY.ToString()}
                        } },
                        { "PAGER" , new BsonDocument{
                            { "LIMIT" , 10000000 },
                            { "PAGE" , 0 }
                        } }
                    };
                var contents = await _genericRepository.GetAllData(document);
                IDictionary<string, string> temp_key_contraints_from_db = new Dictionary<string, string>();

                if (contents != null)
                {
                    foreach (var item in contents["result"]["items"] as BsonArray)
                    {
                        JObject temp = JObject.Parse(item.ToString());
                        if (constraints_from_module_info != null && (constraints_from_module_info.Count > 0))
                        {
                            string key = Utils.Util<string>.getConstraintValueForItem(temp, constraints_from_module_info);
                            if (!temp_key_contraints_from_db.ContainsKey(key))
                            {
                                temp_key_contraints_from_db.Add(key, temp[_id_col].ToString());
                            }
                        }
                    }
                }

                IDictionary<string, JObject> key_and_replaced_items = new Dictionary<string, JObject>();

                foreach (KeyValuePair<string, ActionStatus> item in ItemsStatus)
                {
                    if (item.Value.status == "look_for_constraints")
                    {
                        if (temp_key_contraints_from_db.ContainsKey(item.Value.unique_key))
                        {
                            ValidationErrors duplication_error = new ValidationErrors();

                            duplication_error.errors.Add("Duplicate item in import - " + temp_key_contraints_from_db[item.Value.unique_key]);
                            item.Value.status = "error";
                            item.Value.errors = duplication_error;
                        }
                        else
                        {

                            var temp_item = Information["items"].FirstOrDefault(dict => item.Value.id.ToString().Trim().ToLower() == dict[_id_col].ToString().Trim().ToLower());

                            string temp_id = temp_item[_id_col].ToString().Trim().ToLower();
                            string id = string.Empty;
                            var is_new_item = Item_constraints.FirstOrDefault(dict => dict.Value.ContainsKey("id") && dict.Value["id"].ToString().Trim().ToLower() == temp_id);
                            if (is_new_item.Value != null && is_new_item.Value["is_temp"].ToString().ToLower() == "true")
                            {
                                string query_model_name = "client_models_counter";
                                if (model.Contains("__"))
                                {
                                    string[] info = model.Split("__");
                                    if (info.Length > 0)
                                    {
                                        query_model_name = info[0] + "__" + "client_models_counter";
                                    }
                                }
                                BsonDocument auto_generate = new BsonDocument
                                    {
                                        { "META_DATA", new BsonDocument{
                                                { "ID",query_model_name},
                                             }
                                        },
                                        { "PARAMS", new BsonDocument{
                                            { "INFORMATION" , BsonDocument.Parse(model_info.ToString()) }}
                                        }
                                    };
                                temp_item[_id_col] = Utils.Util<string>.FormatAutoGeneratedID((JObject)model_info?["modules_information"]?["auto_increment"], long.Parse(_genericRepository.AUTO_Generate_ID(auto_generate).Result["sequence"].ToString()));
                                key_and_replaced_items.Add(temp_id, (JObject)temp_item);

                            }
                            else
                            {
                                item.Value.status = "added";
                            }
                        }
                    }
                }

                foreach (KeyValuePair<string, JObject> item in key_and_replaced_items)
                {
                    string key = item.Key.ToString().Trim().ToLower();
                    string id = item.Value[_id_col].ToString();
                    JObject _item = JObject.Parse(item.Value.ToString());
                    ItemsStatus.Remove(key);
                    ItemsStatus.Add(id, new ActionStatus(id, "added", "", "", ""));
                }

            }
            else
            {
                foreach (KeyValuePair<string, ActionStatus> item in ItemsStatus)
                {
                    if (item.Value.status == "look_for_constraints")
                    {
                        item.Value.status = "added";
                    }
                }
            }




            #endregion



            #region Add/ Update 

            BsonArray bulkInsert = new BsonArray();
            BsonArray bulkUpdate = new BsonArray();
            foreach (JObject item in Information["items"])
            {
                try
                {
                    if (ItemsStatus[item[_id_col].ToString()].status.ToLower() == "added")
                    {
                        item["version"] = version;
                        bulkInsert.Add(BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse(item.ToString())));
                        items_for_add_or_update.Add(new BsonDocument{
                                { _id_col,item[_id_col].ToString().Trim() }
                            });

                    }
                    else if (ItemsStatus[item[_id_col].ToString()].status.ToLower() == "exist")
                    {
                        item["version"] = version;
                        item["_id"] = ItemsStatus[item[_id_col].ToString()].system_id;
                        bulkUpdate.Add(BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse(item.ToString())));
                        items_for_add_or_update.Add(new BsonDocument{
                                { _id_col,item[_id_col].ToString().Trim() }
                            });
                    }
                }
                catch (Exception er) { }

            }
            if (bulkInsert.Count > 0 || bulkUpdate.Count > 0)
            {
                BsonDocument document = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model.Trim().ToLower() },
                                { "VERSION",version },
                                {"IDENTITY",_id_col }
                            }
                        },
                        { "VALUE" , new BsonDocument{
                            { "INSERT" , bulkInsert},
                            {"UPDATE", bulkUpdate}
                            }
                        }
                    };
                _genericRepository.BulkOperation(document);

                // If everything is ok then:
                await _uow.Commit();
            }

            #endregion
            var returnDoc = new BsonDocument();

            BsonDocument updatedItems = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model.Trim().ToLower() },
                                { "VERSION",version },
                                {"IDENTITY",_id_col}
                            }
                        },
                        { "VALUE" , items_for_add_or_update }
                    };

            var content = await _genericRepository.GetByIds(updatedItems, false);
            foreach (BsonDocument b in content)
            {
                if (ItemsStatus[b[_id_col].ToString()].status.ToLower() != "error")
                {
                    if (ItemsStatus[b[_id_col].ToString()].status.ToLower() != "added")
                    {
                        ItemsStatus[b[_id_col].ToString()].status = ItemsStatus[b[_id_col].ToString()].update_on == b["updated"].ToString() ? "Not updated" : "Updated";
                    }
                    ItemsStatus[b[_id_col].ToString()].update_on = b["updated"].ToString();
                }

            }
            returnDoc.Add("status", new BsonArray(ItemsStatus.Select(i => i.Value.ToBsonDocument())));
            returnDoc.Add("items", new BsonArray(content.Select(i => i.ToBsonDocument())));
            if (errorItems.Count > 0)
            {
                returnDoc.Add("error_items", new BsonArray(errorItems.Select(i => i.ToBsonDocument())));
            }
            return await Task.FromResult(returnDoc);
        }
    }
}
