﻿using System;
using System.Threading;
using System.Net.Mime;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.IO;
using Newtonsoft.Json.Linq;
using CoreAPI.Application.Interfaces;
using CoreAPI.Common.Settings;

namespace CoreAPI.Application.Data
{
    public class LoggerOperations
    {
        private ILoggerRepository _loggerRepository;
        private IMongoContext _context;
        private IUnitOfWork _uow;
        private readonly SemaphoreSlim autoGenerationLock = new SemaphoreSlim(1, 1);
        public LoggerOperations(ILoggerRepository loggerRepository, IUnitOfWork uow, IMongoContext context)
        {
            _loggerRepository = loggerRepository;
            _uow = uow;
            _context = context;
        }
        public async Task<BsonDocument> GetModelByID(string model, JToken model_info, string id)
        {
            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim(); ;
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;


            BsonDocument document = new BsonDocument
                {
                    { "META_DATA", new BsonDocument{
                            { "ID",model.Trim().ToLower() },
                            {"IDENTITY",_id_col}
                        }
                    },
                    { "VALUE" , new BsonDocument{
                        { "identity_value" ,  id.ToString() },
                    } }
                };
            var content = await _loggerRepository.GetById(document);
            if (content != null && content[_id_col].ToString().ToLower() != id.ToLower())
            {
                content = null;
            }
            return content;
        }
        public async Task<BsonDocument> Create(string model, JToken model_info, JObject Item)
        {

            string version = "0.0.0";

            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim();
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;



            version = model_info["version"] != null ? model_info["version"].ToString() : version;


            #region Auto generate ID

            string auto_gen_id = model_info?["modules_information"]?["auto_increment"]?["field_name"].ToString().ToLower().Trim();

            if (auto_gen_id != null)
            {
                Item[auto_gen_id] = Guid.NewGuid().ToString();

            }

            #endregion



            var validate = JSONValidator.validateSchema(model_info["meta_data"].ToString(), Item);

            if (validate.errors.Count > 0)
            {
                var result = new BsonDocument
                        {
                            {"CODE",500 },
                            { "EXCEPTION", true},
                            { "ERRORS",  validate.ToBsonDocument()}
                        };
                //var result = new BadRequestObjectResult(validate);
                //result.ContentTypes.Add(MediaTypeNames.Application.Json);
                //return result;
                return await Task.FromResult(result);
            }
            else
            {




                BsonDocument document = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model.Trim().ToLower() },
                                { "VERSION",version },
                                {"IDENTITY",_id_col }
                            }
                        },
                        { "VALUE" , BsonDocument.Parse(Item.ToString()) }
                    };

                _loggerRepository.Add(document);

                await _uow.Commit();

                document = new BsonDocument
                {
                    { "META_DATA", new BsonDocument{
                            { "ID",model.Trim().ToLower() },
                            {"IDENTITY",_id_col}
                        }
                    },
                    { "VALUE" , new BsonDocument{
                        { "identity_value" ,  Item[_id_col].ToString() },
                    } }
                };
                // The product will be added only after commit
                return await Task.FromResult(_loggerRepository.GetById(document).Result);
                //return await _genericRepository.GetById(document);


            }
        }
        public async Task<BsonDocument> Get(string model, JToken model_info, string limit, string page, string sort, string sortBy, string query, string exclude)
        {
            JObject QUERY = null;
            try
            {
                QUERY = JObject.Parse(query);
            }
            catch (Exception e)
            {
                QUERY = new JObject();
            }
            limit = limit ?? "100";
            sort = sort ?? "asc";
            sortBy = sortBy ?? "updated";
            page = page ?? "1";
            exclude = exclude ?? "";
            int _page = 1;
            string _sort = "asc";
            int _limit = 100;
            try
            {
                _limit = int.Parse(limit);
                if (_limit > 1000)
                {
                    _limit = 100;
                }
            }
            catch (Exception e)
            {
            }
            try
            {
                _page = int.Parse(page);
                if (_page < 0)
                {
                    _page = 1;
                }

            }
            catch (Exception e)
            {
            }
            try
            {
                _sort = sort.Trim().ToLower() == "asc" ? "asc" : "desc";

            }
            catch (Exception e)
            {
            }

            string _id_col = model_info?["modules_information"]?["identity"]?.ToString().ToLower().Trim(); ;
            _id_col = String.IsNullOrEmpty(_id_col) ? "id" : _id_col;


            BsonDocument document = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model.Trim().ToLower() },
                                {"IDENTITY",_id_col }
                            }
                        },
                        { "PARAMS", new BsonDocument{
                            {"SORTING" , new BsonDocument
                            {
                                { "SORT" ,_sort },
                                {"SORT_BY", sortBy.Trim().ToLower()}

                            }},
                            { "QUERY" , QUERY.ToString()},
                            {"EXCLUDE" , new BsonDocument {
                                { "FILEDS", exclude }
                            } }


                        } },
                        { "PAGER" , new BsonDocument{
                            { "LIMIT" , _limit },
                            { "PAGE" , --_page }
                        } }
                    };
            return await _loggerRepository.GetAllData(document);

        }
    }
}
