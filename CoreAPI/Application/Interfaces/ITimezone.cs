﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Application.Interfaces
{
    public interface ITimezone: IDisposable
    {
        public DateTime GetUserLocalTime();
        public string GetUserLocalTimeasString();
        public DateTime GetUtcTime(DateTime datetime);
    }
}
