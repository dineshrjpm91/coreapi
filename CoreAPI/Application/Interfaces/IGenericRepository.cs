﻿using System;
using MongoDB.Bson;
namespace CoreAPI.Application.Interfaces
{
    public interface IGenericRepository : IRepository<BsonDocument>
    {
    }
}
