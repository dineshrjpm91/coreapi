﻿using CoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Application.Interfaces
{
    public interface IAuthRepository
    {
        Task<AuthendicatedUser> Login(Users user);
    }
}
