﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;


namespace CoreAPI.Application.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        void Add(TEntity meta_data);

        //void InsertOne(TEntity meta_data, TEntity item);
        void BulkOperation(TEntity meta_data);

        Task<TEntity> GetById(TEntity meta_data);
        Task<TEntity> GetIntenalId(TEntity meta_info);
        //Task<TEntity> GetOne(TEntity meta_data, Expression<Func<TEntity, bool>> expression);
        Task<IEnumerable<TEntity>> GetByIds(TEntity meta_data, bool onlyExist);
        Task<IEnumerable<TEntity>> LookUps(TEntity meta_data, bool onlyExist);

        Task<BsonDocument> GetAllData(TEntity meta_data);
        Task<TEntity> AUTO_Generate_ID(TEntity meta_info);

        Task<BsonDocument> Check_For_Unique_Constraints(TEntity meta_info);

        Task<BsonDocument> Get_Item_By_Unique_Constraints(TEntity meta_info);

        Task<BsonDocument> GetDetails(TEntity meta_data);
        void Update(TEntity meta_data);
        void Replace(TEntity meta_data);
        //void FindOneAndUpdate(TEntity meta_data, Expression<Func<TEntity, bool>> expression, UpdateDefinition<TEntity> update, FindOneAndUpdateOptions<TEntity> option);

        void UpdateOne(TEntity meta_data, Expression<Func<TEntity, bool>> expression, UpdateDefinition<TEntity> update);
        void Remove(TEntity meta_data);

        void DeleteOne(TEntity meta_data, Expression<Func<TEntity, bool>> expression);

    }
}
