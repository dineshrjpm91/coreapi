﻿using System;
using System.Threading.Tasks;
using MongoDB.Driver;
namespace CoreAPI.Application.Interfaces
{
    public interface IMongoContext : IDisposable
    {
        void AddCommand(Func<Task> func);
        void ClearCommand();
        Task<int> SaveChanges();
        IMongoCollection<T> GetCollection<T>(string name);
    }
}
