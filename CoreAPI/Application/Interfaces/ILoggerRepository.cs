﻿using System;
using MongoDB.Bson;
namespace CoreAPI.Application.Interfaces
{
    public interface ILoggerRepository : IRepository<BsonDocument>
    {
    }
}
