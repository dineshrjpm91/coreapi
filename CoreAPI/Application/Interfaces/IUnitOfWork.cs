﻿using System;
using System.Threading.Tasks;

namespace CoreAPI.Application.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        Task<bool> Commit();
    }
}
