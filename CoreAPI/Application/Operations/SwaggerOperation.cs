﻿using Microsoft.AspNetCore.Http;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Application.Operations
{
    public class SwaggerOperation : IOperationFilter
    {
        private static readonly string[] fileParameters = new[] { "ContentType", "ContentDisposition", "Headers", "Length", "Name", "FileName" };
        private void RemoveExistingFileParameters(IList<OpenApiParameter> operationParameters)
        {
            foreach (var parameter in operationParameters.Where(p => p.In == 0 && fileParameters.Contains(p.Name)).ToList())
            {
                operationParameters.Remove(parameter);
            }
        }
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (context.ApiDescription.ActionDescriptor.DisplayName.ToLower().IndexOf("ileupload") != -1)
            {
                Console.WriteLine(context.ApiDescription.ActionDescriptor.DisplayName.ToLower());
            }

            var operationHasFileUploadButton = context.ApiDescription.ActionDescriptor.Parameters.Any(x => x.ParameterType == typeof(IFormFile) || x.ParameterType == typeof(IFormFileCollection));

            if (!operationHasFileUploadButton)
            {
                return;
            }
            var fileProperties = new OpenApiSchema()
            {
                Description = "Select file",
                Type = "string",
                Format = "binary"

            };

            if (context.ApiDescription.ActionDescriptor.Parameters.Any(x => x.ParameterType == typeof(IFormFileCollection)) == true)
            {
                fileProperties = new OpenApiSchema()
                {
                    Description = "Select file(s)",
                    Type = "array",
                    Items = new OpenApiSchema()
                    {
                        Type = "string",
                        Format = "binary"
                    }

                };
            }

            RemoveExistingFileParameters(operation.Parameters);
            operation.RequestBody = new OpenApiRequestBody()
            {
                Content =
                    {
                        ["multipart/form-data"] = new OpenApiMediaType()
                        {
                            Schema = new OpenApiSchema()
                            {
                                Type = "object",
                                Properties =
                                {
                                    ["file"] = fileProperties

                                }

                                //Properties =
                                //{
                                //    ["file"] = new OpenApiSchema()
                                //    {

                                //        Items = new OpenApiSchema()
                                //        {
                                //            Type = "string",
                                //            Format = "binary"
                                //        },
                                //        Description = "Select file(s)", Type = "array",

                                //    }

                                //}
                            }
                        }
                    }
            };
        }
    }
}
