﻿using CoreAPI.Application.Interfaces;
using CoreAPI.Application.Repository;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Application.Operations
{
    public class DataBaseOperations
    {
        GenericRepository _DBConnections;
        //IUnitOfWork _uow;
        public DataBaseOperations(IMongoContext context, ITimezone timezone)
        {
            _DBConnections = new GenericRepository(context, timezone);
            // _uow = new UoW.UnitOfWork(context);
        }
        public BsonDocument getData(BsonDocument query)
        {
            return _DBConnections.GetAllData(query).Result;
        }
        public void saveData(BsonDocument upadte)
        {
            _DBConnections.BulkOperation(upadte);
        }
        public List<BsonDocument> LookUps(BsonDocument query)
        {
            return (List<BsonDocument>)_DBConnections.LookUps(query, true).Result;
        }
    }
}
