﻿using CoreAPI.Application.Interfaces;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoreAPI.Application.Repository
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly IMongoContext Context;
        protected IMongoCollection<TEntity> DbSet;
        /// <summary>  
        /// The user culture  
        /// </summary>  
        protected readonly ITimezone Timezone;

        protected BaseRepository(IMongoContext context)
        {
            Context = context;
        }
        protected BaseRepository(IMongoContext context, ITimezone timeZone)
        {
            Context = context;
            Timezone = timeZone;
        }
        public virtual async Task<TEntity> GetById(IMongoCollection<TEntity> DbSet, string id_field, string id_value)
        {
            var projection = Builders<TEntity>.Projection
            .Exclude("password")
            .Exclude("_id"); // _id is special and needs to be explicitly excluded if not needed


            var options = new FindOptions<TEntity> { Projection = projection };
            var filters = Builders<TEntity>.Filter.Empty;
            /*
            BsonDocument bson = new BsonDocument
            {
                { id_field , new BsonDocument{ { "$regex", id_value } ,{ "$options" , "i"} } }
            };

            try
            {
                filters = BsonSerializer.Deserialize<BsonDocument>(bson);
            }
            catch (Exception e)
            {
                filters = Builders<TEntity>.Filter.Empty;
            }
             var data = await DbSet.FindAsync(filters, options);
             */
            var data = await DbSet.FindAsync(Builders<TEntity>.Filter.Eq(id_field, BsonRegularExpression.Create(new Regex(id_value, RegexOptions.IgnoreCase))), options);
            return data.FirstOrDefault();
        }

        public virtual void Add(TEntity meta_info)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());
            JObject contentToUpdate = (JObject)j["VALUE"];
            try
            {
                if (contentToUpdate["created"] == null)
                {
                    contentToUpdate.Add(new JProperty("created", ""));
                }
                if (contentToUpdate["updated"] == null)
                {
                    contentToUpdate.Add(new JProperty("updated", ""));
                }
                contentToUpdate["created"] = Timezone.GetUserLocalTimeasString();//DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                contentToUpdate["updated"] = Timezone.GetUserLocalTimeasString();//DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception e) { }

            Context.AddCommand(() => DbSet.InsertOneAsync(BsonSerializer.Deserialize<TEntity>(BsonDocument.Parse(contentToUpdate.ToString()))));
        }

        private void ConfigDbSet(string name)
        {
            DbSet = Context.GetCollection<TEntity>(name);
        }


        public virtual async Task<TEntity> GetById(TEntity meta_info)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());
            var projection = Builders<TEntity>.Projection
                .Exclude("password")
                .Exclude("_id"); // _id is special and needs to be explicitly excluded if not needed
            var options = new FindOptions<TEntity> { Projection = projection };

            string identity = j?["META_DATA"]?["IDENTITY"]?.ToString();
            identity = String.IsNullOrEmpty(identity) ? "id" : identity;

            string identity_value = j["VALUE"]["identity_value"] != null ? j["VALUE"]["identity_value"].ToString() : j["VALUE"][identity].ToString();


            //var data = await DbSet.FindAsync(Builders<TEntity>.Filter.Eq(identity, BsonRegularExpression.Create(new Regex(identity_value, RegexOptions.IgnoreCase))), options);
            //return data.FirstOrDefault();
            return await GetById(DbSet, identity, identity_value);

        }
        public virtual async Task<BsonDocument> Get_Item_By_Unique_Constraints(TEntity meta_info)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());
            string __identity = j?["META_DATA"]?["IDENTITY"]?.ToString();
            JArray fields = (JArray)j["PARAMS"]["FIELD_SETS"];

            JObject identifier = new JObject();

            BsonDocument result = new BsonDocument();

            if (j["PARAMS"] != null && j["PARAMS"]["IDENTIFIER"] != null)
            {
                identifier = JObject.Parse(j["PARAMS"]["IDENTIFIER"].ToString());
            }

            var projection = Builders<TEntity>.Projection
                .Exclude("_id");
            IDictionary<string, string> contraint_id_key_values = new Dictionary<string, string>();
            foreach (var field in fields)
            {
                string id = field["ID"].ToString().Trim();
                if (!contraint_id_key_values.ContainsKey(id))
                {
                    contraint_id_key_values.Add(id, field["VALUE"].ToString().Trim());
                }
            }
            var filters = Builders<TEntity>.Filter.Empty;
            try
            {
                filters = BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse(j["PARAMS"]["QUERY"].ToString()));
            }
            catch (Exception e)
            {
                filters = Builders<TEntity>.Filter.Empty;
            }

            var options = new FindOptions<TEntity> { Projection = projection };

            var data = await DbSet.FindAsync(filters, options);
            var list = data.ToList();
            if (list.Count == 0)
            {
                return null;
            }
            else
            {
                return list[0].ToBsonDocument();
            }

        }
        public virtual async Task<BsonDocument> Check_For_Unique_Constraints(TEntity meta_info)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());
            string __identity = j?["META_DATA"]?["IDENTITY"]?.ToString();
            JArray fields = (JArray)j["PARAMS"]["FIELD_SETS"];

            JObject identifier = new JObject();

            BsonDocument result = new BsonDocument();

            if (j["PARAMS"] != null && j["PARAMS"]["IDENTIFIER"] != null)
            {
                identifier = JObject.Parse(j["PARAMS"]["IDENTIFIER"].ToString());
            }

            var projection = Builders<TEntity>.Projection
                .Exclude("_id");
            IDictionary<string, string> contraint_id_key_values = new Dictionary<string, string>();
            foreach (var field in fields)
            {
                string id = field["ID"].ToString().Trim();
                if (!contraint_id_key_values.ContainsKey(id))
                {
                    contraint_id_key_values.Add(id, field["VALUE"].ToString().Trim());
                }
            }
            var filters = Builders<TEntity>.Filter.Empty;
            try
            {
                filters = BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse(j["PARAMS"]["QUERY"].ToString()));
            }
            catch (Exception e)
            {
                filters = Builders<TEntity>.Filter.Empty;
            }

            var options = new FindOptions<TEntity> { Projection = projection };

            var data = await DbSet.FindAsync(filters, options);
            var list = data.ToList();

            if (list.Count == 0)
            {
                result = new BsonDocument
                {
                    { "EXIST", false},
                    { "SAME_ITEM", false}
                };
                return result;
            }
            else
            {
                var resultDocument = list[0].ToBsonDocument();

                string exiting_item_id = "";



                JObject item = JObject.Parse(resultDocument.ToString());
                try
                {
                    if (__identity != null)
                    {
                        exiting_item_id = (string)item.SelectToken(__identity);
                    }

                }
                catch (Exception e)
                {

                }

                if (identifier["ID"] == null)
                {

                    int count = fields.Count;

                    BsonArray error = new BsonArray();

                    foreach (var field in fields)
                    {

                        string id = field["ID"].ToString().Trim();
                        string Key_Value = (string)item.SelectToken(id);
                        string item_ID = exiting_item_id + " - ";
                        if (Key_Value != null)
                        {
                            if (Key_Value.Trim().ToString().ToLower() == contraint_id_key_values[id].Trim().ToString().ToLower())
                            {
                                count = count - 1;
                                error.Add(new BsonDocument { { "name", id }, { "message", item_ID + Key_Value + " - duplicate key" } });
                            }
                        }
                    }

                    if (count == fields.Count)
                    {
                        result = new BsonDocument
                        {
                            { "EXIST", false}
                        };
                    }
                    else
                    {
                        result = new BsonDocument
                        {
                            { "EXIST", true},
                            {"CODE",409 },
                            {"EXIST_ITEM_ID",exiting_item_id },
                            { "EXCEPTION", true},
                            {"ERRORS" ,error }
                        };

                    }
                }
                else
                {
                    string id = identifier["ID"].ToString().ToLower();
                    string value = identifier["VALUE"].ToString().ToUpper();
                    string Key_Value = (string)item.SelectToken(id);
                    if (Key_Value.ToString().Trim().ToUpper() != value)
                    {
                        BsonArray error = new BsonArray();
                        error.Add(new BsonDocument { { "item_exist", id }, { "message", Key_Value + " - duplicate key" } });
                        result = new BsonDocument
                        {
                            { "SAME_ITEM", false},
                            {"EXIST_ITEM_ID",exiting_item_id },
                            { "EXIST", true},
                            {"CODE",409 },
                            { "EXCEPTION", true},
                            {"ERRORS",  error}
                        };
                    }
                    else
                    {
                        result = new BsonDocument
                        {
                            { "EXIST", true},
                            {"EXIST_ITEM_ID",exiting_item_id },
                            { "SAME_ITEM", true}
                        };
                    }
                }

                return result;
            }


        }
        public virtual async Task<TEntity> AUTO_Generate_ID(TEntity meta_info)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());

            JObject module_info = (JObject)j["PARAMS"]["INFORMATION"];

            string field_id = module_info["id"].ToString().ToLower() + "_" + module_info["modules_information"]["auto_increment"]["field_name"].ToString().ToLower();

            if (module_info["modules_information"]["auto_increment"] != null && module_info["modules_information"]["auto_increment"]["auto_generation_field_id"] != null)
            {
                field_id = module_info["modules_information"]["auto_increment"]["auto_generation_field_id"].ToString().ToLower();
            }


            var filter = new BsonDocument("id", field_id);
            var update = Builders<TEntity>.Update.Inc("sequence", 1);

            var options = new FindOneAndUpdateOptions<TEntity>
            {
                ReturnDocument = ReturnDocument.After
            };
            var data = await DbSet.FindOneAndUpdateAsync(filter, update, options);

            if (data == null)
            {
                JObject content_to_insert = new JObject();
                content_to_insert["id"] = field_id;
                content_to_insert["sequence"] = 0;
                try
                {
                    content_to_insert["sequence"] = long.Parse(module_info["modules_information"]["auto_increment"]["start"].ToString());
                }
                catch (Exception e)
                {

                }
                var insert_data = DbSet.InsertOneAsync(BsonSerializer.Deserialize<TEntity>(BsonDocument.Parse(content_to_insert.ToString())));

                data = await DbSet.FindOneAndUpdateAsync(filter, update, options);
            }


            return data;
        }

        public virtual async Task<TEntity> GetIntenalId(TEntity meta_info)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());
            string identity = j?["META_DATA"]?["IDENTITY"]?.ToString();
            identity = String.IsNullOrEmpty(identity) ? "id" : identity;
            var projection = Builders<TEntity>.Projection
                    .Include("updated")
                    .Include(identity); // _id is special and needs to be explicitly excluded if not needed
            var options = new FindOptions<TEntity> { Projection = projection };
            string identity_value = j["VALUE"]["identity_value"] != null ? j["VALUE"]["identity_value"].ToString() : j["VALUE"][identity].ToString();
            var data = await DbSet.FindAsync(Builders<TEntity>.Filter.Eq(identity, identity_value), options);

            return data.SingleOrDefault();
        }
        public virtual async Task<BsonDocument> GetDetails(TEntity meta_info)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());
            var projection = Builders<TEntity>.Projection
                .Exclude("_id") // _id is special and needs to be explicitly excluded if not needed
                .Exclude("password");
            var options = new FindOptions<TEntity> { Projection = projection };
            var data = await DbSet.CountDocumentsAsync(Builders<TEntity>.Filter.Empty);
            BsonDocument bson = new BsonDocument
            {
                {"result", new BsonDocument{ { "total", data.ToString() } } }
            };
            return bson;
        }


        public virtual async Task<IEnumerable<TEntity>> LookUps(TEntity meta_info, bool onlyExist)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());

            string identity = j?["META_DATA"]?["IDENTITY"]?.ToString();
            identity = String.IsNullOrEmpty(identity) ? "id" : identity;

            var projection = Builders<TEntity>.Projection
                .Exclude("_id") // _id is special and needs to be explicitly excluded if not needed
                .Exclude("password");
            if (onlyExist)
            {
                projection = Builders<TEntity>.Projection
                    .Include("updated")
                    .Include(identity);
            }

            var filters = Builders<TEntity>.Filter.Empty;
            try
            {
                filters = BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse(j["PARAMS"]["QUERY"].ToString()));
            }
            catch (Exception e)
            {
                filters = Builders<TEntity>.Filter.Empty;
            }

            var options = new FindOptions<TEntity> { Projection = projection };

            var data = await DbSet.FindAsync(filters, options);
            return data.ToList();
        }
        public virtual async Task<IEnumerable<TEntity>> GetByIds(TEntity meta_info, bool onlyExist)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());

            string identity = j?["META_DATA"]?["IDENTITY"]?.ToString();
            identity = String.IsNullOrEmpty(identity) ? "id" : identity;


            var projection = Builders<TEntity>.Projection
                .Exclude("_id") // _id is special and needs to be explicitly excluded if not needed
                .Exclude("password");
            if (onlyExist)
            {
                projection = Builders<TEntity>.Projection
                    .Include("updated")
                    .Include(identity);
            }
            var options = new FindOptions<TEntity> { Projection = projection };
            List<string> d = new List<string>();
            foreach (JObject content in j["VALUE"])
            {

                try
                {
                    d.Add(content[identity].ToString());
                }
                catch (Exception e)
                {

                }
            }
            var data = await DbSet.FindAsync(Builders<TEntity>.Filter.In(identity, d), options);
            return data.ToList();
        }

        public virtual async Task<BsonDocument> GetAllData(TEntity meta_info)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());
            //var query = new QueryDocument(doc);


            var sort = Builders<TEntity>.Sort.Descending($"updated");

            try
            {
                string SORT = j["PARAMS"]["SORTING"]["SORT"].ToString();
                string SORT_BY = "updated";
                try
                {
                    SORT_BY = j["PARAMS"]["SORTING"]["SORT_BY"].ToString();
                }


                catch (Exception e) { }
                //SORT_BY = "$" + SORT_BY;
                string[] SORT_BY_ARRAY = SORT_BY.Split(',');



                if (SORT == "asc")
                {
                    sort = Builders<TEntity>.Sort.Ascending(SORT_BY_ARRAY[0].Trim());
                    if (SORT_BY_ARRAY.Length > 1)
                    {
                        for (int i = 1; i < SORT_BY_ARRAY.Length; i++)
                        {
                            if (SORT_BY_ARRAY[i].Trim().ToLower() != string.Empty)
                            {
                                sort.Ascending(SORT_BY_ARRAY[i].Trim().ToLower());
                            }

                        }

                    }

                }
                else
                {
                    sort = Builders<TEntity>.Sort.Descending(SORT_BY_ARRAY[0].Trim());
                    for (int i = 1; i < SORT_BY_ARRAY.Length; i++)
                    {
                        if (SORT_BY_ARRAY[i].Trim().ToLower() != string.Empty)
                        {
                            sort.Descending(SORT_BY_ARRAY[i].Trim().ToLower());
                        }
                    }

                }


            }
            catch (Exception e) { }
            //{ x: 10, y: { $lt: 20 } }
            var filters = Builders<TEntity>.Filter.Empty;
            try
            {
                filters = BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse(j["PARAMS"]["QUERY"].ToString()));
            }
            catch (Exception e)
            {
                filters = Builders<TEntity>.Filter.Empty;
            }
            var projection = Builders<TEntity>.Projection
                .Exclude("_id") // _id is special and needs to be explicitly excluded if not needed
                .Exclude("password");

            try
            {
                if (j["PARAMS"]["EXCLUDE"] != null && j["PARAMS"]["EXCLUDE"]["FILEDS"] != null)
                {
                    string exclude = j["PARAMS"]["EXCLUDE"]["FILEDS"].ToString().Trim().ToLower();
                    if (exclude != string.Empty)
                    {
                        string[] EXCLUDE_ARRAY = exclude.Split(',');
                        for (int i = 0; i < EXCLUDE_ARRAY.Length; i++)
                        {
                            if (EXCLUDE_ARRAY[i].Trim().ToLower() != string.Empty)
                            {
                                projection = projection.Exclude(EXCLUDE_ARRAY[i].Trim());
                            }

                        }
                    }
                }

            }
            catch (Exception e) { }
            int skip = 0;
            int limit = (int)j["PAGER"]["LIMIT"];
            int page = (int)j["PAGER"]["PAGE"];
            if (page <= 0)
            {
                page = 1;
                skip = 0;
            }
            else
            {

                skip = (limit * page);
                ++page;

            }



            var options = new FindOptions<TEntity>
            {
                Projection = projection,
                Limit = (int)j["PAGER"]["LIMIT"],
                Skip = skip,
                Sort = sort

            };


            var all = await DbSet.FindAsync(filters, options);
            var total = await DbSet.CountDocumentsAsync(filters);
            BsonDocument bson = new BsonDocument
            {
                {"result", new BsonDocument {
                        { "meta_info", new BsonDocument{
                            { "total", total},
                            { "limit", (int)j["PAGER"]["LIMIT"]},
                            { "page", page},
                        } },
                        { "items", Utils.Util<TEntity>.ToBsonDocumentArray(all.ToList())}
                    }
                }
            };
            return bson;


            //  return all.ToList();
        }
        public virtual void Replace(TEntity meta_info)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());

            string identity = j?["META_DATA"]?["IDENTITY"]?.ToString();
            identity = String.IsNullOrEmpty(identity) ? "id" : identity;


            JObject contentToUpdate = (JObject)j["VALUE"];
            try
            {
                if (contentToUpdate["updated"] == null)
                {
                    contentToUpdate.Add(new JProperty("updated", ""));
                }
                contentToUpdate["updated"] = Timezone.GetUserLocalTimeasString();//DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception e) { }
            //var projection = Builders<TEntity>.Projection
            //    .Exclude("_id"); // _id is special and needs to be explicitly excluded if not needed
            //var options = new FindOptions<TEntity> { Projection = projection };



            string identity_value = j["VALUE"]["identity_value"] != null ? j["VALUE"]["identity_value"].ToString() : j["VALUE"][identity].ToString();

            Context.AddCommand(() => DbSet.ReplaceOneAsync(Builders<TEntity>.Filter.Eq(identity, BsonRegularExpression.Create(new Regex(identity_value, RegexOptions.IgnoreCase))), BsonSerializer.Deserialize<TEntity>(BsonDocument.Parse(contentToUpdate.ToString()))));
        }
        public virtual void Update(TEntity meta_info)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());

            string identity = j?["META_DATA"]?["IDENTITY"]?.ToString();
            identity = String.IsNullOrEmpty(identity) ? "id" : identity;


            var content_formating = ((JObject)j["VALUE"]).ToString();

            if (content_formating.Contains("$ref"))
            {
                content_formating = content_formating.Replace("[$ref]", "$ref");
                content_formating = content_formating.Replace("$ref", "[$ref]");

            }
            JObject contentToUpdate = JObject.Parse(content_formating);

            JObject contentFromDB = contentToUpdate;

            string identity_value = j["VALUE"]["identity_value"] != null ? j["VALUE"]["identity_value"].ToString() : j["VALUE"][identity].ToString();

            var projection = Builders<TEntity>.Projection
                .Exclude("_id"); // _id is special and needs to be explicitly excluded if not needed
            var options = new FindOptions<TEntity> { Projection = projection };

            //var data = DbSet.FindAsync(Builders<TEntity>.Filter.Eq(identity, BsonRegularExpression.Create(new Regex(identity_value, RegexOptions.IgnoreCase))), options).Result.FirstOrDefault();
            var data = GetById(DbSet, identity, identity_value).Result;
            try
            {

                contentFromDB = JObject.Parse(data.ToString());
                if (contentFromDB[identity].ToString().ToLower() != identity_value.ToLower())
                {
                    throw new Exception(identity_value + " - Item not found");
                }
            }
            catch (Exception e)
            {


            }

            contentFromDB.Merge(contentToUpdate, new JsonMergeSettings
            {
                // union array values together to avoid duplicates
                MergeArrayHandling = MergeArrayHandling.Replace,
                MergeNullValueHandling = MergeNullValueHandling.Merge

            });

            contentToUpdate = (JObject)Utils.Util<TEntity>.RemoveEmptyChildren(contentFromDB);
            try
            {
                if (contentToUpdate["updated"] == null)
                {
                    contentToUpdate.Add(new JProperty("updated", ""));
                }
                contentToUpdate["updated"] = Timezone.GetUserLocalTimeasString();//DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception e) { }
            //var projection = Builders<TEntity>.Projection
            //    .Exclude("_id"); // _id is special and needs to be explicitly excluded if not needed

            var _content = contentToUpdate.ToString();



            var update = new BsonDocument() { { "$set", BsonDocument.Parse(_content) } };

            Context.AddCommand(() => DbSet.UpdateOneAsync(Builders<TEntity>.Filter.Eq(identity, BsonRegularExpression.Create(new Regex(identity_value, RegexOptions.IgnoreCase))), update, new UpdateOptions { IsUpsert = true }));
            //options: new UpdateOptions { IsUpsert = true }
        }
        public virtual void Remove(TEntity meta_info)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());

            string identity = j?["META_DATA"]?["IDENTITY"]?.ToString();
            identity = String.IsNullOrEmpty(identity) ? "id" : identity;
            var projection = Builders<TEntity>.Projection
                    .Include("updated")
                    .Include(identity); // _id is special and needs to be explicitly excluded if not needed
            var options = new FindOptions<TEntity> { Projection = projection };
            string identity_value = j["VALUE"]["identity_value"] != null ? j["VALUE"]["identity_value"].ToString() : j["VALUE"][identity].ToString();
            var data = DbSet.FindAsync(Builders<TEntity>.Filter.Eq(identity, identity_value), options).Result;
            foreach (var item in data.ToList())
            {
                DbSet.DeleteOne((Builders<TEntity>.Filter.Eq("_id", item.ToBsonDocument()["_id"].ToString())));
            }
            Context.AddCommand(() => DbSet.DeleteManyAsync(Builders<TEntity>.Filter.Eq(identity, j["VALUE"]["identity_value"].ToString())));
        }


        public virtual async Task<TEntity> GetOne(TEntity meta_info, Expression<Func<TEntity, bool>> expression)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());
            var data = await DbSet.FindAsync(expression);
            return data.SingleOrDefault();
        }

        public virtual void FindOneAndUpdate(TEntity meta_info, Expression<Func<TEntity, bool>> expression, UpdateDefinition<TEntity> update, FindOneAndUpdateOptions<TEntity> option)
        {
            JObject j = JObject.Parse(meta_info.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());

            string identity = j?["META_DATA"]?["IDENTITY"]?.ToString();
            identity = String.IsNullOrEmpty(identity) ? "id" : identity;


            JObject contentToUpdate = (JObject)j["VALUE"];
            try
            {
                if (contentToUpdate["updated"] == null)
                {
                    contentToUpdate.Add(new JProperty("updated", ""));
                }
                contentToUpdate["updated"] = Timezone.GetUserLocalTimeasString();//DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception e) { }

            Context.AddCommand(() => DbSet.FindOneAndUpdateAsync(expression, update, option));
        }


        public void UpdateOne(TEntity meta_data, Expression<Func<TEntity, bool>> expression, UpdateDefinition<TEntity> update)
        {
            throw new NotImplementedException();
        }

        public void DeleteOne(TEntity meta_data, Expression<Func<TEntity, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public void BulkOperation(TEntity meta_data)
        {

            JObject j = JObject.Parse(meta_data.ToString());
            ConfigDbSet(j["META_DATA"]["ID"].ToString());


            string identity = j?["META_DATA"]?["IDENTITY"]?.ToString();
            identity = String.IsNullOrEmpty(identity) ? "id" : identity;


            if (j["VALUE"]["INSERT"] != null)
            {
                JArray items = j["VALUE"]["INSERT"] as JArray;
                if (items.Count > 0)
                {
                    foreach (JObject content in items)
                    {

                        try
                        {
                            if (content["created"] == null)
                            {
                                content.Add(new JProperty("created", ""));
                            }
                            if (content["updated"] == null)
                            {
                                content.Add(new JProperty("updated", ""));
                            }
                            content["created"] = Timezone.GetUserLocalTimeasString();//DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            content["updated"] = Timezone.GetUserLocalTimeasString();//DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        }
                        catch (Exception e)
                        {

                        }
                    }

                    Context.AddCommand(() => DbSet.InsertManyAsync(BsonSerializer.Deserialize<IEnumerable<TEntity>>(j["VALUE"]["INSERT"].ToString())));

                }

            }

            if (j["VALUE"]["UPDATE"] != null)
            {
                JArray items = j["VALUE"]["UPDATE"] as JArray;

                if (items.Count > 0)
                {
                    var bulkOps = new List<WriteModel<TEntity>>();

                    foreach (JObject content in items)
                    {

                        try
                        {
                            if (content["created"] == null)
                            {
                                content.Add(new JProperty("created", ""));
                                content["created"] = Timezone.GetUserLocalTimeasString();//DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            }
                            if (content["updated"] == null)
                            {
                                content.Add(new JProperty("updated", ""));
                            }

                            content["updated"] = Timezone.GetUserLocalTimeasString();//DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        }
                        catch (Exception e)
                        {
                        }

                        BsonDocument bson = new BsonDocument();

                        bson["_id"] = ObjectId.Parse(content["_id"].ToString());
                        bson = BsonSerializer.Deserialize<BsonDocument>(content.ToString());
                        bson.Remove("_id");
                        bson.InsertAt(0, new BsonElement("_id", ObjectId.Parse(content["_id"].ToString())));
                        var filter = new FilterDefinitionBuilder<TEntity>().Eq("_id", bson["_id"]);

                        filter = BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse("{\"" + identity + "\":\"" + content[identity].ToString() + "\"}"));
                        var upsertOne = new UpdateOneModel<TEntity>(
                            filter,
                            new BsonDocument { { "$set", bson } })
                        { IsUpsert = true };
                        bulkOps.Add(upsertOne);

                    }

                    Context.AddCommand(() => DbSet.BulkWriteAsync(bulkOps));
                }

            }

            //DbSet.InsertManyAsync(BsonSerializer.Deserialize<IEnumerable<TEntity>>(j["VALUE"].ToString()));
        }


        public void Dispose()
        {
            Context?.Dispose();
            Timezone?.Dispose();
        }
    }
}
