﻿using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using CoreAPI.Application.Interfaces;
using CoreAPI.Models;

namespace CoreAPI.Application.Repository
{
    public class AuthRepository : IAuthRepository
    {
        protected IMongoCollection<Users> UsersSet;
        protected IMongoCollection<Roles> RolesSet;
        public AuthRepository(IMongoContext context)
        {
            UsersSet = context.GetCollection<Users>("pellucid_users");
            RolesSet = context.GetCollection<Roles>("pellucid_user_roles");
        }

        public Roles getUserRole(string role_id)
        {
            var query = RolesSet.Find(role => (role.id.ToUpper() == role_id.ToUpper()))
                                .FirstOrDefaultAsync();
            return query.Result;
        }
        public Users getUserInfo(string user_id)
        {
            var query = UsersSet.Find(user => (user.id.ToUpper() == user_id.ToUpper()))
                                .FirstOrDefaultAsync();
            return query.Result;
        }

        public async Task<AuthendicatedUser> Login(Users u)
        {
            var authUser = AuthendicatedUser.CreateAsync("unknown", "",-1,"anonymn", new full_name { first_name="",last_name=""},"",""); 
            var query = UsersSet.Find(user => ( user.id.ToUpper() == u.id.ToUpper() || user.email.ToUpper() == u.id.ToUpper()) && user.user_status == "active")
                                .FirstOrDefaultAsync();

            try
            {
                if (query.Result != null)
                {
                    if (SecurePasswordHasher.Verify(u.password, query.Result.password))
                    {
                        var update = new MongoDB.Bson.BsonDocument() { { "$set", new MongoDB.Bson.BsonDocument() { { 
                            "last_login",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)
                            } } } };
                        try
                        {
                            var userLoginUpdate = UsersSet.UpdateOneAsync(Builders<Users>.Filter.Eq("id", query.Result.id), update, new UpdateOptions { IsUpsert = true }).Result;
                        }
                        catch(Exception e)
                        {

                        }
                        
                        authUser = AuthendicatedUser.CreateAsync(u.id, System.Guid.NewGuid().ToString(), query.Result.user_type,query.Result.user_role, query.Result.full_name,query.Result.client_id,query.Result.email);
                    }

                }

            }
            catch(Exception e)
            {
                throw e;
            }
            
            return await authUser;
        }
    }
}
