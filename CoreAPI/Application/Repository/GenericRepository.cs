﻿using CoreAPI.Application.Interfaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Application.Repository
{
    public class GenericRepository : BaseRepository<BsonDocument>, IGenericRepository
    {
        public GenericRepository(IMongoContext context) : base(context)
        {
        }
        public GenericRepository(IMongoContext context, ITimezone timeZone) : base(context, timeZone)
        {
        }
    }
}
