﻿using Microsoft.Extensions.Configuration;
using CoreAPI.Application.Interfaces;
using System;


namespace CoreAPI.Application.Repository
{

    /// <summary>  
    /// User's culture information.  
    /// </summary>  
    public class TimezoneRepository : ITimezone
    {
        private readonly IConfiguration _configuration;

        /// <summary>  
        /// Initializes a new instance of the <see cref="TimezoneRepository"/> class.  
        /// </summary>  
        public TimezoneRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            // TODO: Need to through DB Context.  
            //TimeZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            TimeZone = TimeZoneConverter.TZConvert.GetTimeZoneInfo(_configuration["Timezone:Countryzone"]);
            //DateTimeFormat = "M/d/yyyy h:m:ss tt"; // Default format.  
            DateTimeFormat = _configuration["Timezone:DatetimeFormat"];
        }
        public TimezoneRepository(string zone,string format)
        {
            // TODO: Need to through DB Context.  
            //TimeZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            TimeZone = TimeZoneConverter.TZConvert.GetTimeZoneInfo(zone);
            //DateTimeFormat = "M/d/yyyy h:m:ss tt"; // Default format.  
            DateTimeFormat = format;
        }
        /// <summary>  
        /// Gets or sets the date time format.  
        /// </summary>  
        /// <value>  
        /// The date time format.  
        /// </value>  
        public string DateTimeFormat { get; set; }
        /// <summary>  
        /// Gets or sets the time zone.  
        /// </summary>  
        /// <value>  
        /// The time zone.  
        /// </value>  
        public TimeZoneInfo TimeZone { get; set; }
        /// <summary>  
        /// Gets the user local time.  
        /// </summary>  
        /// <returns></returns>  
        public DateTime GetUserLocalTime()
        {
            return TimeZoneInfo.ConvertTime(DateTime.UtcNow, TimeZone);
        }
        /// <summary>  
        /// Gets the user local time.  
        /// </summary>  
        /// <returns></returns>  
        public string GetUserLocalTimeasString()
        {
            return TimeZoneInfo.ConvertTime(DateTime.UtcNow, TimeZone).ToString(DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
        }
        /// <summary>  
        /// Gets the UTC time.  
        /// </summary>  
        /// <param name="datetime">The datetime.</param>  
        /// <returns>Get universal date time based on User's Timezone</returns>  
        public DateTime GetUtcTime(DateTime datetime)
        {
            return TimeZoneInfo.ConvertTime(datetime, TimeZone).ToUniversalTime();
        }
        public void Dispose()
        {            
            GC.SuppressFinalize(this);
        }
    }

}
