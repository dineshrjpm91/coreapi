﻿using CoreAPI.Application.Interfaces;
using MongoDB.Bson;



namespace CoreAPI.Application.Repository
{
    public class LoggerRepository : BaseRepository<BsonDocument>, ILoggerRepository
    {
        public LoggerRepository(IMongoContext context) : base(context)
        {
        }
	    public LoggerRepository(IMongoContext context, ITimezone timeZone) : base(context, timeZone)
        {
        }
    }
}