﻿using CoreAPI.Application.Interfaces;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Application.Contexts
{
    public class MongoContext : IMongoContext
    {
        private IMongoDatabase Database { get; set; }
        public IClientSessionHandle Session { get; set; }
        public MongoClient MongoClient { get; set; }
        private readonly List<Func<Task>> _commands;
        private readonly IConfiguration _configuration;

        public MongoContext(IConfiguration configuration)
        {
            _configuration = configuration;


            // Every command will be stored and it'll be processed at SaveChanges
            _commands = new List<Func<Task>>();
        }

        public async Task<int> SaveChanges()
        {
            ConfigureMongo();

            using (Session = await MongoClient.StartSessionAsync())
            {
                Session.StartTransaction();

                var commandTasks = _commands.Select(c => c());

                await Task.WhenAll(commandTasks);

                await Session.CommitTransactionAsync();
            }

            return _commands.Count;
        }
        public void ClearCommand()
        {
            _commands.Clear();
        }
        private void ConfigureMongo()
        {
            if (MongoClient != null)
                return;

            // Configure mongo (You can inject the config, just to simplify)
            var environment_db_connection = Environment.GetEnvironmentVariable("DB_CONNECTION_MONGO");
            var environment_db_name = Environment.GetEnvironmentVariable("DB_NAME");
            if (environment_db_connection == null || environment_db_name == null)
            {
                MongoClient = Application.Backend.GetMongoClient(_configuration["MongoDB:Connection"]);
                Database = MongoClient.GetDatabase(_configuration["MongoDB:Database"]);
            }
            else
            {
                MongoClient = Application.Backend.GetMongoClient(environment_db_connection);
                Database = MongoClient.GetDatabase(environment_db_name);
            }


            // MongoClient = new MongoClient(_configuration["MongoDB:Connection"]);
            //Database = Application.Backend.GetMongoDatabase(_configuration["MongoDB:Connection"], _configuration["MongoDB:Database"]);

        }


        public IMongoCollection<T> GetCollection<T>(string name)
        {
            ConfigureMongo();
            return Database.GetCollection<T>(name);
        }

        public void Dispose()
        {
            Session?.Dispose();
            GC.SuppressFinalize(this);
        }

        public void AddCommand(Func<Task> func)
        {
            _commands.Add(func);
        }
    
    
    }
}
