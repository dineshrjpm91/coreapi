﻿using CoreAPI.Common.Settings;
using Microsoft.Extensions.Options;
using MongoDB.Driver;


namespace CoreAPI.Application
{
    public class Backend
    {
        private static IMongoDatabase _database = null;
        private static MongoClient _mongoclient = null;

        static Backend()
        {
        }
        public static IMongoDatabase GetMongoDatabase(IOptions<MongoDBSettings> settings)
        {
            if (_database == null)
            {
                _mongoclient = new MongoClient(settings.Value.ConnectionString);
                if (_mongoclient != null)
                    _database = _mongoclient.GetDatabase(settings.Value.Database);
            }
            return _database;
        }
        public static IMongoDatabase GetMongoDatabase(string Connection, string Database)
        {
            if (_database == null)
            {
                _mongoclient = new MongoClient(Connection);
                if (_mongoclient != null)
                    _database = _mongoclient.GetDatabase(Database);
            }
            return _database;
        }
        public static MongoClient GetMongoClient(string Connection)
        {
            if (_mongoclient == null)
            {
                _mongoclient = new MongoClient(Connection);
            }
            return _mongoclient;
        }
        public static IMongoDatabase GetMongoDatabase( string Database)
        {
            return _mongoclient.GetDatabase(Database);
        }
    }

}
