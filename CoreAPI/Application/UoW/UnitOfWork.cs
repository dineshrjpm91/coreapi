﻿using CoreAPI.Application.Interfaces;
using System.Threading.Tasks;


namespace CoreAPI.Application.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IMongoContext _context;

        public UnitOfWork(IMongoContext context)
        {
            _context = context;
        }

        public async Task<bool> Commit()
        {
            var changeAmount = await _context.SaveChanges();
            _context.ClearCommand();
            return changeAmount > 0;
        }
        public void ClearCommand()
        {
            _context.ClearCommand();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
