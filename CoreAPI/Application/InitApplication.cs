﻿using CoreAPI.Application.Contexts;
using CoreAPI.Application.Interfaces;
using CoreAPI.Application.Operations;
using CoreAPI.Application.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Application
{
    public class InitApplication
    {
        private IWebHostEnvironment env;       
        private IConfiguration config;
        private IMongoContext context;
        private ITimezone timeZone;
        private readonly IUnitOfWork _uow;
        public InitApplication(IWebHostEnvironment env, IConfiguration configuration)
        {
            this.env = env;
            this.config = configuration;
            context = new MongoContext(config);
            timeZone = new TimezoneRepository(config);
            _uow = new UoW.UnitOfWork(context);
        }
        public void buildApplicationSettings()
        {
            string applicationBasePath = config.GetSection("Application:BasePath").Value;
            string basePathReadFromCommand = config.GetValue<string>("base_url");

            if (!String.IsNullOrEmpty(basePathReadFromCommand))
            {
                applicationBasePath = basePathReadFromCommand;
            }
            try
            {
                loadClients();
            }
            catch (Exception e)
            {
                string s = e.Message + ":__:" + e.StackTrace;
                throw new Exception(s);
            }

            updateSettings(applicationBasePath);
            updateSwaggerSettings(applicationBasePath);

        }

        JObject getInformationFromDB(string model_name)
        {
            BsonDocument document = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model_name },
                                {"IDENTITY","id"}
                            }
                        },
                        { "PARAMS", new BsonDocument{
                            {"SORTING" , new BsonDocument
                            {
                                { "SORT" ,"asc" },
                                {"SORT_BY", "updated"}

                            }},
                            { "QUERY" , ""}


                        } },
                        { "PAGER" , new BsonDocument{
                            { "LIMIT" , 10000 },
                            { "PAGE" , 0 }
                        } }
                    };

            return JObject.Parse(BsonExtensionMethods.ToJson(new DataBaseOperations(context, timeZone).getData(document), new JsonWriterSettings { OutputMode = JsonOutputMode.Strict }));
        }
        void createSystemSetting(string model_name, string seed_data, string file_name)
        {
            var _setting_base_path = env.ContentRootPath + "//apps//";
            BsonDocument document = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model_name },
                                {"IDENTITY","id"}
                            }
                        },
                        { "PARAMS", new BsonDocument{
                            {"SORTING" , new BsonDocument
                            {
                                { "SORT" ,"asc" },
                                {"SORT_BY", "updated"}

                            }},
                            { "QUERY" , ""}


                        } },
                        { "PAGER" , new BsonDocument{
                            { "LIMIT" , 10000 },
                            { "PAGE" , 0 }
                        } }
                    };
            var dbOpt = new DataBaseOperations(context, timeZone);
            var meta_data = dbOpt.getData(document);

            if (meta_data["result"]["meta_info"]["total"] == 0)
            {
                Console.WriteLine("Import default models ::" + model_name);


                JArray models_list = new JArray();

                try
                {
                    models_list = JArray.Parse(File.ReadAllText(_setting_base_path + seed_data));
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error loading - Init Client INFO :: " + e.InnerException.ToString());
                }

                if (models_list.Count != 0)
                {
                    var bulkInsert = new BsonArray();
                    foreach (JObject item in models_list)
                    {
                        bulkInsert.Add(BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse(item.ToString())));
                    }
                    if (bulkInsert.Count > 0)
                    {
                        var insert = new BsonDocument
                                {
                                    { "META_DATA", new BsonDocument{
                                            { "ID",model_name },
                                            {"IDENTITY","id"}
                                        }
                                    },
                                    { "VALUE" , new BsonDocument{
                                        { "INSERT" , bulkInsert},
                                        {"UPDATE", new BsonArray()}
                                        }
                                    }
                                };
                        dbOpt.saveData(insert);
                    }

                }
                meta_data = dbOpt.getData(document);
            }



            JObject cModels = JObject.Parse(BsonExtensionMethods.ToJson(meta_data, new JsonWriterSettings { OutputMode = JsonOutputMode.Strict }));
            File.WriteAllText(_setting_base_path + file_name, cModels["result"]["items"].ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));
        }

        void loadClients()
        {


            //IUnitOfWork _uow = new UoW.UnitOfWork(context);
            var _setting_base_path = env.ContentRootPath + "//apps//";

            #region Deployment models

            JObject deploymentModels = getInformationFromDB("pellucid_deployment_meta_data");
            if ((long)deploymentModels["result"]["meta_info"]["total"] == 0)
            {
                //change function name to deployment settings
                createSystemSetting("pellucid_deployment_meta_data", "default-app-deployment.json", "app_deployment-publised.json");

            }
            if (File.Exists(_setting_base_path + "app_deployment-publised.json"))
            {
                JArray deploymentInfo = JArray.Parse(File.ReadAllText(_setting_base_path + "app_deployment-publised.json"));
                if (deploymentInfo.Count() == 0)
                {
                    createSystemSetting("pellucid_deployment_meta_data", "default-app-deployment.json", "app_deployment-publised.json");
                }
            }
            #endregion Deployment models

            #region Users and roles - Import

            JObject roles = getInformationFromDB("pellucid_user_roles");

            if ((long)roles["result"]["meta_info"]["total"] == 0)
            {
                importDefaultRoles();
            }

            JObject users = getInformationFromDB("pellucid_users");
            if ((long)users["result"]["meta_info"]["total"] == 0)
            {
                importDefaultUsers();
            }

            #endregion Users and roles - Import

            #region Init clients

            JObject clients = getInformationFromDB("pellucid_clients");
            if ((long)clients["result"]["meta_info"]["total"] == 0)
            {

                JArray _clients = baseSetupSteps();

            }

            #endregion Init clients


            /// Load client with app settings.
            if (!File.Exists(_setting_base_path + "clients.json"))
            {
                if (File.Exists(_setting_base_path + "default-clients.json"))
                {
                    JArray clientsList = JArray.Parse(File.ReadAllText(_setting_base_path + "default-clients.json"));
                    JObject clients_minimum_info = new JObject();
                    string fileServerBasePath = Environment.GetEnvironmentVariable("PELLUCID_LOCAL_PATH");
                    foreach (JObject client in clientsList)
                    {
                        string clientDB_Prefix = client["id"].ToString().Trim().ToLower();

                        #region Administration Create/Load



                        JObject clientsAdministration = getInformationFromDB(clientDB_Prefix + "__" + "pellucid_administration_meta_data");

                        if ((long)clientsAdministration["result"]["meta_info"]["total"] == 0)
                        {
                            importClientInformation(clientDB_Prefix + "__" + "pellucid_administration_meta_data", "default-administration-models.json");

                        }


                        #endregion Administration Create/Load

                        #region Base models Create/Load


                        JObject clientsBaseModels = getInformationFromDB(clientDB_Prefix + "__" + "pellucid_meta_data");

                        if ((long)clientsBaseModels["result"]["meta_info"]["total"] == 0)
                        {
                            importClientInformation(clientDB_Prefix + "__" + "pellucid_meta_data", "default-clients-models.json");

                        }


                        #endregion Base models Create/Load
                    }
                }
            }


            _uow.Commit();


            publishAll();





        }

        void publishAll()
        {
            var _setting_base_path = env.ContentRootPath + "//apps//";

            #region Publish Deployment

            JObject deploymentModels = getInformationFromDB("pellucid_deployment_meta_data");

            if (!File.Exists(_setting_base_path + "app_deployment-publised.json"))
            {
                File.WriteAllText(_setting_base_path + "app_deployment-publised.json", deploymentModels["result"]["items"].ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));
            }

            #endregion Publish Deployment

            #region Publish clients

            JObject clients = getInformationFromDB("pellucid_clients");
            if ((long)clients["result"]["meta_info"]["total"] > 0)
            {
                File.WriteAllText(_setting_base_path + "clients.json", clients["result"]["items"].ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));
            }
            #endregion Publish clients


            if (File.Exists(_setting_base_path + "clients.json"))
            {
                JArray clientsList = JArray.Parse(File.ReadAllText(_setting_base_path + "clients.json"));
                JObject clients_minimum_info = new JObject();
                string fileServerBasePath = Environment.GetEnvironmentVariable("PELLUCID_LOCAL_PATH");
                foreach (JObject client in clientsList)
                {
                    string clientDB_Prefix = client["id"].ToString().Trim().ToLower();
                    string clientFolder = @"wwwroot/application/clients/" + client["information"]["file_system_base_path"].ToString().Trim().ToLower() + "/";
                    string file_base_path = @"wwwroot/files/" + client["information"]["file_system_base_path"].ToString().Trim().ToLower() + "/";
                    string clientConfigPath = Path.Combine(fileServerBasePath, clientFolder);
                    string clientFilePath = Path.Combine(fileServerBasePath, file_base_path);

                    clients_minimum_info[clientDB_Prefix] = JObject.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(new { name = client["name"], active = client["is_active"], settings = clientConfigPath, assets_path = clientFilePath }));



                    Utils.IOOperations.checkAndCreateDirectory(clientConfigPath);
                    Utils.IOOperations.checkAndCreateDirectory(clientFilePath);


                    #region Administration Create/Load



                    JObject clientsAdministration = getInformationFromDB(clientDB_Prefix + "__" + "pellucid_administration_meta_data");

                    File.WriteAllText(clientConfigPath + "administration.json", clientsAdministration["result"]["items"].ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));
                    if (!File.Exists(clientConfigPath + "administration-published.json"))
                    {
                        File.WriteAllText(clientConfigPath + "administration-published.json", clientsAdministration["result"]["items"].ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));
                        JObject cAdminModelsKeyPair = new JObject();
                        foreach (JObject item in clientsAdministration["result"]["items"])
                        {
                            cAdminModelsKeyPair[item["id"].ToString().Trim()] = item;
                        }
                        File.WriteAllText(clientConfigPath + "administration-kp-published.json", cAdminModelsKeyPair.ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));
                    }


                    // Load the information in session server

                    #endregion Administration Create/Load

                    #region Base models Create/Load


                    JObject clientsBaseModels = getInformationFromDB(clientDB_Prefix + "__" + "pellucid_meta_data");

                    File.WriteAllText(clientConfigPath + "client-models.json", clientsBaseModels["result"]["items"].ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));

                    if (!File.Exists(clientConfigPath + "client-models-published.json"))
                    {
                        File.WriteAllText(clientConfigPath + "client-models-published.json", clientsBaseModels["result"]["items"].ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));
                        JObject clientsBaseModelsKeyPair = new JObject();
                        foreach (JObject item in clientsBaseModels["result"]["items"])
                        {
                            clientsBaseModelsKeyPair[item["id"].ToString().Trim()] = item;
                        }
                        File.WriteAllText(clientConfigPath + "client-models-kp-published.json", clientsBaseModelsKeyPair.ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));
                    }

                    #endregion Base models Create/Load

                    #region Base app settings




                    if (client["settings"] != null)
                    {
                        File.WriteAllText(clientConfigPath + "app-settings.json", client.ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));
                    }
                    else
                    {
                        client["settings"] = JObject.Parse(File.ReadAllText(_setting_base_path + "pellucid-default-apps.json"));
                        File.WriteAllText(clientConfigPath + "app-settings.json", client.ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));
                    }
                    #endregion Base app settings
                }
                File.WriteAllText(_setting_base_path + "client_minimum_info.json", clients_minimum_info.ToString(Newtonsoft.Json.Formatting.Indented).Replace("[$ref]", "$ref"));
            }



        }
        void importClientInformation(string model_name, string seed_data)
        {
            var _setting_base_path = env.ContentRootPath + "//apps//";
            BsonDocument document = new BsonDocument
                    {
                        { "META_DATA", new BsonDocument{
                                { "ID",model_name },
                                {"IDENTITY","id"}
                            }
                        },
                        { "PARAMS", new BsonDocument{
                            {"SORTING" , new BsonDocument
                            {
                                { "SORT" ,"asc" },
                                {"SORT_BY", "updated"}

                            }},
                            { "QUERY" , ""}


                        } },
                        { "PAGER" , new BsonDocument{
                            { "LIMIT" , 10000 },
                            { "PAGE" , 0 }
                        } }
                    };


            var dbOpt = new DataBaseOperations(context, timeZone);
            var meta_data = dbOpt.getData(document);

            if (meta_data["result"]["meta_info"]["total"] == 0)
            {
                Console.WriteLine("Import default models ::" + model_name);


                JArray models_list = new JArray();

                try
                {
                    models_list = JArray.Parse(File.ReadAllText(_setting_base_path + seed_data));
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error loading - Init Client INFO :: " + e.InnerException.ToString());
                }

                if (models_list.Count != 0)
                {

                    var bulkInsert = new BsonArray();
                    foreach (JObject item in models_list)
                    {
                        bulkInsert.Add(BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse(item.ToString())));
                    }
                    if (bulkInsert.Count > 0)
                    {
                        var insert = new BsonDocument
                                {
                                    { "META_DATA", new BsonDocument{
                                            { "ID",model_name },
                                            {"IDENTITY","id"}
                                        }
                                    },
                                    { "VALUE" , new BsonDocument{
                                        { "INSERT" , bulkInsert},
                                        {"UPDATE", new BsonArray()}
                                        }
                                    }
                                };
                        dbOpt.saveData(insert);
                    }
                }
            }
        }

        private void importDefaultRoles()
        {
            var _setting_base_path = env.ContentRootPath + "//apps//";
            JArray roles = new JArray();
            try
            {
                roles = JArray.Parse(File.ReadAllText(_setting_base_path + "default-roles.json"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading - default roles :: " + e.InnerException.ToString());
            }
            if (roles.Count != 0)
            {
                JArray _roles = new JArray();
                foreach (JObject item in roles)
                {
                    _roles.Add(item["username"]?.ToString());
                }
                JObject roleSearch = new JObject();

                roleSearch["id"] = new JObject();
                roleSearch["id"]["$in"] = _roles;

                var dbOpt = new DataBaseOperations(context, timeZone);

                var rolesSearchInfo = dbOpt.LookUps(CreationTemplating("pellucid_user_roles", "username", roleSearch));

                if (rolesSearchInfo.Count() == 0)
                {
                    Console.WriteLine("Creating roles");


                    BsonArray bulkInsert = new BsonArray();

                    foreach (JObject item in roles)
                    {
                        if (rolesSearchInfo.Where(role => role["id"] == item["id"].ToString().ToLower()).Count() == 0)
                        {
                            bulkInsert.Add(BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse(item.ToString())));
                        }
                    }
                    if (bulkInsert.Count > 0)
                    {
                        BsonDocument updateDocs = new BsonDocument
                        {
                            { "META_DATA", new BsonDocument{
                                    { "ID","pellucid_user_roles" },
                                    {"IDENTITY","username"}
                                }
                            },
                            { "VALUE" , new BsonDocument{
                                { "INSERT" , bulkInsert},
                                {"UPDATE", new BsonArray()}
                                }
                            }
                        };
                        dbOpt.saveData(updateDocs);
                    }

                }
                else
                {
                    Console.WriteLine("Roles exist");

                }

            }
        }

        private void importDefaultUsers()
        {

            var _setting_base_path = env.ContentRootPath + "//apps//";
            JArray users_list = new JArray();


            try
            {
                users_list = JArray.Parse(File.ReadAllText(_setting_base_path + "default-users.json"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading - default users :: " + e.InnerException.ToString());
            }
            if (users_list.Count != 0)
            {
                var dbOpt = new DataBaseOperations(context, timeZone);
                JArray _users = new JArray();
                foreach (JObject item in users_list)
                {
                    _users.Add(item["username"]?.ToString());
                }
                JObject userSearch = new JObject();

                userSearch["username"] = new JObject();
                userSearch["username"]["$in"] = _users;

                var usersSearchInfo = dbOpt.LookUps(CreationTemplating("pellucid_users", "username", userSearch));

                if (usersSearchInfo.Count() == 0)
                {
                    Console.WriteLine("Creating users");


                    BsonArray bulkInsert = new BsonArray();

                    foreach (JObject item in users_list)
                    {
                        if (usersSearchInfo.Where(user => user["username"].ToString().ToLower() == item["username"].ToString().ToLower()).Count() == 0)
                        {
                            item["password"] = Models.SecurePasswordHasher.Hash(item["password"].ToString());
                            bulkInsert.Add(BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse(item.ToString())));
                        }
                    }
                    if (bulkInsert.Count > 0)
                    {
                        BsonDocument updateDocs = new BsonDocument
                        {
                            { "META_DATA", new BsonDocument{
                                    { "ID","pellucid_users" },
                                    {"IDENTITY","username"}
                                }
                            },
                            { "VALUE" , new BsonDocument{
                                { "INSERT" , bulkInsert},
                                {"UPDATE", new BsonArray()}
                                }
                            }
                        };
                        dbOpt.saveData(updateDocs);

                    }
                }
                else
                {
                    Console.WriteLine("User exist");

                }

            }

        }
        BsonDocument CreationTemplating(string model_name, string identity, JObject serach)
        {
            return new BsonDocument
                {
                    { "META_DATA", new BsonDocument{
                            { "ID",model_name },
                            {"IDENTITY",identity }
                        }
                    },
                    { "PARAMS", new BsonDocument{
                            { "QUERY" , serach.ToString()}
                    } }
                 };
        }
        private JArray baseSetupSteps()
        {

            var _setting_base_path = env.ContentRootPath + "//apps//";


            JArray clients = new JArray();

            try
            {
                clients = JArray.Parse(File.ReadAllText(_setting_base_path + "default-clients.json"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading - default clients :: " + e.InnerException.ToString());
            }

            if (clients.Count != 0)
            {

                var dbOpt = new DataBaseOperations(context, timeZone);
                JArray _clients = new JArray();
                foreach (JObject item in clients)
                {
                    _clients.Add(item["id"]?.ToString());
                }
                JObject _clientsSearch = new JObject();

                _clientsSearch["id"] = new JObject();
                _clientsSearch["id"]["$in"] = _clients;
                var clientSearch = dbOpt.LookUps(CreationTemplating("pellucid_clients", "id", _clientsSearch));

                if (clientSearch.Count() == 0)
                {
                    Console.WriteLine("Creating base clients");



                    BsonArray bulkInsert = new BsonArray();

                    foreach (JObject item in clients)
                    {
                        if (clientSearch.Where(client => client["id"].ToString().ToLower() == item["id"].ToString().ToLower()).Count() == 0)
                        {
                            bulkInsert.Add(BsonSerializer.Deserialize<BsonDocument>(BsonDocument.Parse(item.ToString())));
                        }
                    }
                    if (bulkInsert.Count > 0)
                    {

                        BsonDocument updateDocs = new BsonDocument
                        {
                            { "META_DATA", new BsonDocument{
                                    { "ID","pellucid_clients" },
                                    {"IDENTITY","id"}
                                }
                            },
                            { "VALUE" , new BsonDocument{
                                { "INSERT" , bulkInsert},
                                {"UPDATE", new BsonArray()}
                                }
                            }
                        };
                        dbOpt.saveData(updateDocs);

                    }

                }
                else
                {
                    Console.WriteLine("Default clients already exist");

                }
            }

            return clients;


        }

        public void updateSettings(string basePath)
        {
            var _setting_base_path = env.ContentRootPath + "//apps//";
            JObject system_models = new JObject();


            JArray systemInfo = JArray.Parse(File.ReadAllText(_setting_base_path + "system.json"));
            foreach (JObject system in systemInfo)
            {
                system_models[system["id"].ToString().Trim()] = system;
            }
            #region Deployment models
            JArray deploymentInfo = JArray.Parse(File.ReadAllText(_setting_base_path + "app_deployment-publised.json"));
            JObject deployment_models = new JObject();
            foreach (JObject admin in deploymentInfo)
            {
                deployment_models[admin["id"].ToString().Trim()] = admin;
            }
            #endregion Deployment models
            #region Administration models

            JArray administrationInfo = JArray.Parse(File.ReadAllText(_setting_base_path + "default-administration-models.json"));
            JObject administration_models = new JObject();
            foreach (JObject admin in administrationInfo)
            {
                administration_models[admin["id"].ToString().Trim()] = admin;
            }
            #endregion Administration models
            JObject settings = new JObject();
            settings["settings"] = new JObject();
            JObject application = new JObject();
            JObject ClientsList = new JObject();
            try
            {
                ClientsList = JObject.Parse(File.ReadAllText(_setting_base_path + "client_minimum_info.json"));
            }
            catch (Exception e)
            {


            }
            application["BASE_PATH"] = basePath;

            try
            {
                application["BASE_PATH"] = basePath;
                application["APP_AUTH_PATH"] = basePath + config.GetSection("TokenAuthentication:TokenPath").Value;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading - Token info :: " + e.InnerException.ToString());
            }
            try
            {
                application["TIME_ZONE"] = new JObject();
                application["TIME_ZONE"]["COUNTRY_ZONE"] = config.GetSection("Timezone:Countryzone").Value;
                application["TIME_ZONE"]["DATE_FORMAT"] = config.GetSection("Timezone:DatetimeFormat").Value;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error Setting Timezone :: " + e.InnerException.ToString());
            }

            JArray ecommerceInfo = JArray.Parse(File.ReadAllText(_setting_base_path + "default-payment-models.json"));
            JObject ecommerce_models = new JObject();
            foreach (JObject ecom in ecommerceInfo)
            {
                ecommerce_models[ecom["id"].ToString().Trim()] = ecom;
            }

            settings["settings"]["SYSTEM"] = system_models.ToString(Newtonsoft.Json.Formatting.None);
            settings["settings"]["CLIENTS"] = ClientsList.ToString(Newtonsoft.Json.Formatting.None);
            settings["settings"]["APPLICATION"] = application.ToString(Newtonsoft.Json.Formatting.None);
            settings["settings"]["ADMINISTRATION"] = administration_models.ToString(Newtonsoft.Json.Formatting.None);
            settings["settings"]["DEPLOYMENT"] = deployment_models.ToString(Newtonsoft.Json.Formatting.None);
            settings["settings"]["ECOMMERCE"] = ecommerce_models.ToString(Newtonsoft.Json.Formatting.None);
            File.WriteAllText(_setting_base_path + "settings.json", settings.ToString(Newtonsoft.Json.Formatting.Indented));


        }
        void updateSwaggerSettings(string basePath)
        {
            
            var rootPathFile = env.ContentRootPath + "//wwwroot//swagger//dist//path-info.js";
            File.WriteAllText(rootPathFile, " var PELLUCID_SERVER_APP_BASE_PATH = '" + basePath + "';");
        }

    }
}
