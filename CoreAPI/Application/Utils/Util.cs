﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Newtonsoft.Json.Linq;
using MongoDB.Bson.IO;
using MongoDB.Bson;

namespace CoreAPI.Application.Utils
{
    public static class IOOperations
    {
        public static void checkAndCreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }
    public static class IDFormator
    {
        public static string ReplaceInvalidChars(string id)
        {
            return System.Text.RegularExpressions.Regex.Replace(id, "[^a-zA-Z0-9_-]+", "_", System.Text.RegularExpressions.RegexOptions.Compiled);
        }
    }

    public static class FileOperations
    {
        public static string ReplaceInvalidChars(string filename)
        {
            return System.Text.RegularExpressions.Regex.Replace(filename, "[^a-zA-Z0-9_.]+", "_", System.Text.RegularExpressions.RegexOptions.Compiled);
        }
        public static string getFileContentType(string file_name)
        {
            var provider = new FileExtensionContentTypeProvider();
            string contentType;
            if (!provider.TryGetContentType(file_name, out contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }
        public static string getExtension(string file_name)
        {
            return Path.GetExtension(file_name).ToLower();
        }
    }
    public static class RequestHandling
    {
        public static object formatRequest(HttpContext request)
        {
            var features = request.Features.Get<IHttpRequestFeature>();
            return new { action = new { trace_identifier = request?.TraceIdentifier, url = $"{features.Scheme}://{request?.Request?.Host.Value}{features.RawTarget}", method = request?.Request?.Method, query_string = request?.Request?.QueryString.ToUriComponent() }, client_info = new { http_referer = request?.Request?.Headers["Referer"].ToString(), remote_ip = request?.Connection?.RemoteIpAddress?.ToString(), local_ip = request?.Connection?.LocalIpAddress?.ToString() } }; ;
        }
    }
    public static class Util<T>
    {
        public static string getPatientFullName(JObject fullname)
        {
            string return_value = string.Empty;
            if(fullname["first_name"] != null)
            {
                return_value = return_value + fullname["first_name"].ToString();
            }
            if (fullname["middle_name"] != null)
            {
                return_value = return_value +"_" + fullname["middle_name"].ToString();
            }
            if (fullname["last_name"] != null)
            {
                return_value = return_value + "_" + fullname["last_name"].ToString();
            }
            return return_value;
        }
        public static string getPatientMRN(JObject patient_info)
        {
            string return_value = string.Empty;
            if (patient_info["patient_mrn"] != null)
            {
                return_value = return_value + patient_info["patient_mrn"].ToString();
            }
            return return_value;
        }
        public static string getConstraintValueForItem(JObject item, JArray constraints_from_module_info)
        {
            string return_value = string.Empty;
            foreach (var contraints in constraints_from_module_info)
            {
                string __id = contraints.ToString().Trim().ToLower();

                string Key_Value = (string)item.SelectToken(__id);
                if (Key_Value != null)
                {
                    return_value = return_value + Key_Value.Trim().ToLower() + "_";
                }

            }
            return return_value;
        }
        public static JObject formatItem(string identity_column, JObject Item, string model, JArray list_of_contraints, JObject item_identifier)
        {
            BsonArray check_constraint_keys = new BsonArray();
            JArray constraints_from_module_info = list_of_contraints;
            foreach (var constraint in constraints_from_module_info)
            {
                BsonDocument key = new BsonDocument();
                string __id = constraint.ToString().Trim().ToLower();

                string Key_Value = (string)Item.SelectToken(__id);
                key["ID"] = __id;
                key["VALUE"] = Key_Value != null ? Key_Value.Trim() : string.Empty;
                if(Key_Value != null)
                {
                    Item.SelectToken(__id).Replace(Key_Value.Trim());
                }
            }
            return Item;
        }
        public static BsonDocument formalateUniqueConstraint(string identity_column,JObject Item,string model, JArray list_of_contraints, JObject item_identifier)
        {
            
            BsonArray check_constraint_keys = new BsonArray();
            JObject constraint_query = new JObject();
            JArray constraints_from_module_info = list_of_contraints;
            foreach (var constraint in constraints_from_module_info)
            {
                BsonDocument key = new BsonDocument();
                string __id = constraint.ToString().Trim().ToLower();

                string Key_Value = (string)Item.SelectToken(__id);
                key["ID"] = __id;
                key["VALUE"] = Key_Value != null ? Key_Value.Trim() : string.Empty;
                check_constraint_keys.Add(key);
                JObject qry = new JObject();
                qry["$regex"] = key["VALUE"].ToString();
                qry["$options"] = "i";
                constraint_query[__id] = qry;
            }
            BsonDocument check_constraint = new BsonDocument
            {
                { "META_DATA", new BsonDocument{
                        { "ID",model.Trim().ToLower() },
                        {"IDENTITY",identity_column },
                        }
                },
                { "PARAMS", new BsonDocument{
                        { "FIELD_SETS" , check_constraint_keys },
                        { "QUERY" , constraint_query.ToString() }
                    }
                }

            };
            if (item_identifier != null)
            {
                check_constraint["PARAMS"]["IDENTIFIER"] = BsonDocument.Parse(item_identifier.ToString());
            }
            return check_constraint;
        }

        public static string FormatAutoGeneratedID(JObject information, long sequence)
        {
            string rv = sequence.ToString();
            string prefix = string.Empty;
            string suffix = string.Empty;
            int length = 0;
            if (information["prefix"] != null)
            {
                prefix = information["prefix"].ToString();
            }
            if (information["suffix"] != null)
            {
                suffix = information["suffix"].ToString();
            }
            if (information["length"] != null)
            {
                try
                {
                    length = int.Parse(information["length"].ToString());
                }
                catch (Exception e) { }
            }
            if(length != 0)
            {
                rv = sequence.ToString("D" + length.ToString());
            }
            return prefix + rv + suffix;
        }

        public static JObject BsontoJson(BsonDocument doc)
        {
            var jsonWriterSettings = new JsonWriterSettings { OutputMode = JsonOutputMode.Strict };
            return JObject.Parse("{}");
        }
        public static BsonArray ToBsonDocumentArray(IEnumerable<T> list)
        {
            var array = new BsonArray();
            foreach (var item in list)
            {
                array.Add(item.ToBsonDocument());
            }
            return array;
        }
        public static JToken RemoveEmptyChildren(JToken token)
        {
            if (token.Type == JTokenType.Object)
            {
                JObject copy = new JObject();
                foreach (JProperty prop in token.Children<JProperty>())
                {
                    JToken child = prop.Value;
                    if (child.HasValues)
                    {
                        child = RemoveEmptyChildren(child);
                    }
                    if (!IsEmpty(child))
                    {
                        copy.Add(prop.Name, child);
                    }
                }
                return copy;
            }
            else if (token.Type == JTokenType.Array)
            {
                JArray copy = new JArray();
                foreach (JToken item in token.Children())
                {
                    JToken child = item;
                    if (child.HasValues)
                    {
                        child = RemoveEmptyChildren(child);
                    }
                    if (!IsEmpty(child))
                    {
                        copy.Add(child);
                    }
                }
                return copy;
            }
            return token;
        }

        public static bool IsEmpty(JToken token)
        {
            return (token.Type == JTokenType.Null);
        }
    }
}
