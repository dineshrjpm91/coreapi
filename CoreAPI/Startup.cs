using CoreAPI.Application;
using CoreAPI.Application.Contexts;
using CoreAPI.Application.Interfaces;
using CoreAPI.Application.Repository;
using CoreAPI.Common.Memory;
using CoreAPI.Installer;
using CoreAPI.Providers;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreAPI
{
    public partial class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IWebHostEnvironment env)
        {

            var args = Environment.GetCommandLineArgs().Skip(1).ToArray();
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddCommandLine(args)
                .AddJsonFile("apps/settings.json", optional: false, reloadOnChange: true)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables(); 

            Configuration = builder.Build();

            var initInfo = new InitApplication(env, Configuration);
            initInfo.buildApplicationSettings();

         }
       

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //  services.AddControllers();

            services.Configure<Common.Settings.ApplicationSettings>(Configuration.GetSection("settings"));

            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue;
                x.MultipartHeadersLengthLimit = int.MaxValue;
            });


            services.InstallServiesInAssemby(Configuration);
            RegisterServices(services);

            services.AddAntiforgery(options =>
            {
                // Set Cookie properties using CookieBuilder properties†.
                options.FormFieldName = "csrf-token";
                options.HeaderName = "X-CSRF-TOKEN";
                options.SuppressXFrameOptionsHeader = false;
            });
        }
        private void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IMongoContext, MongoContext>();
            services.AddScoped<IUnitOfWork, Application.UoW.UnitOfWork>();
            services.AddScoped<IGenericRepository, GenericRepository>();
            services.AddScoped<IAuthRepository, AuthRepository>();
            services.AddScoped<ILoggerRepository, LoggerRepository>();
            services.AddScoped<ITimezone, TimezoneRepository>();
            services.AddSingleton<MemCache>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IAntiforgery antiforgery)
        {

            String basePathReadFromCommand = Configuration.GetValue<string>("base_url");

            app.UsePathBase(basePathReadFromCommand);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }
            else
            {
                app.UseStatusCodePagesWithReExecute("/error/{0}");
                //app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            //Maintain the Session
            app.UseStaticFiles();
            app.UseSession();
            app.UseCookiePolicy();


            app.Use(next => context =>
            {
                string path = context.Request.Path.Value;


                if (
                    string.Equals(path, "/", StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(path, basePathReadFromCommand + Configuration.GetSection("TokenAuthentication:TokenPath").Value, StringComparison.OrdinalIgnoreCase))
                {
                    // The request token can be sent as a JavaScript-readable cookie, 
                    // and Angular uses it by default.
                    var tokens = antiforgery.GetAndStoreTokens(context);
                    context.Response.Cookies.Append("XSRF-TOKEN", tokens.RequestToken,
                        new CookieOptions() { HttpOnly = false });
                }

                return next(context);
            });

            //Add JWToken to all incoming HTTP Request Header from session
            app.Use(async (context, next) =>
            {
                var JWToken = context.Session.GetString("JWToken");
                
                
                if (!string.IsNullOrEmpty(JWToken))
                {
                    try
                    {
                        context.Request.Headers.Add("Authorization", "Bearer " + JWToken);
                    }
                    catch (Exception e) { }
                }

                await next();



                if (context.Response.StatusCode == 404 && !context.Response.HasStarted)
                {
                    //Re-execute the request so the user gets the error page
                    string originalPath = context.Request.Path.Value;
                    context.Items["originalPath"] = originalPath;
                    context.Request.Path = "/error/404";
                    await next();
                }
                if (context.Response.StatusCode == 500 && !context.Response.HasStarted)
                {
                    //Re-execute the request so the user gets the error page
                    string originalPath = context.Request.Path.Value;
                    context.Items["originalPath"] = originalPath;
                    context.Request.Path = "/error/500";
                    await next();
                }
            });

            app.UseAuthentication();

            string basePath = Environment.GetEnvironmentVariable("PELLUCID_LOCAL_PATH");
            if (basePath != null)
            {
                if (!Directory.Exists(Path.Combine(basePath, @"wwwroot/files/")))
                {
                    Directory.CreateDirectory(Path.Combine(basePath, @"wwwroot/files/"));
                }
                if (Directory.Exists(Path.Combine(basePath, @"wwwroot/files/")))
                {
                    app.UseStaticFiles(new StaticFileOptions()
                    {
                        OnPrepareResponse = (context) =>
                        {
                            if (!context.Context.User.Identity.IsAuthenticated && context.Context.Request.Path.StartsWithSegments("/files"))
                            {
                                throw new Exception("Not authenticated");
                            }
                        },
                        FileProvider = new PhysicalFileProvider(Path.Combine(basePath, @"wwwroot/files/")),
                        RequestPath = new PathString("/files")
                    });

                }

            }
          
           
           
            app.UseMvc();
            app.ConfigServiesInAssemby(env, Configuration);
           

        }

      

    }
}
