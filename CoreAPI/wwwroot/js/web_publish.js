﻿$(document).ready(function () {
    // click on button submit
    $("#submit").on('click', function () {
        // send ajax
        $.ajax({
            url: $("#print_form").attr('action'), // url where to submit the request
            type: "POST", // type of action POST || GET
            data: JSON.stringify($("#print_form").serializeArray().reduce(function (a, x) { a[x.name] = x.value; return a; }, {})),// post data || get data
            contentType: "application/json",
            success: function (result) {
                // you can see the result from the console
                // tab of the developer tools
                var w = window.open('about:blank');
                w.document.open();
                w.document.write(result);
                w.document.close();
            },
            error: function (xhr, resp, text) {
                console.log(xhr, resp, text);
            }
        })
    });


    $("#generate_pdf").on('click', function () {


        $.ajax({
            type: "POST",
            url: $("#print_form").attr('action'),
            data: JSON.stringify($("#print_form").serializeArray().reduce(function (a, x) { a[x.name] = x.value; return a; }, {})),
            contentType: "application/json",
            xhrFields: {
                responseType: 'blob' // to avoid binary data being mangled on charset conversion
            },
            success: function (blob, status, xhr) {
                // check for a filename
                var filename = "";
                var disposition = xhr.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                }

                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                    // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                    window.navigator.msSaveBlob(blob, filename);
                } else {
                    var URL;
                    if (window.URL.createObjectURL) {
                        URL = window.URL;
                    }
                    else {
                        URL = window.webkitURL;
                    }
                    var downloadUrl = URL.createObjectURL(blob);

                    if (filename) {
                        // use HTML5 a[download] attribute to specify filename
                        var a = document.createElement("a");
                        // safari doesn't support this yet
                        if (typeof a.download === 'undefined') {
                            window.location.href = downloadUrl;
                        } else {
                            a.href = downloadUrl;
                            a.download = filename;
                            document.body.appendChild(a);
                            a.click();
                        }
                    } else {
                        window.location.href = downloadUrl;
                    }

                    setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                }
            }
        });


    });
});
