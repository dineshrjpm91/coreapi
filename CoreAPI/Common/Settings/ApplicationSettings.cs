﻿using CoreAPI.Models;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Common.Settings
{
    public class ApplicationSettings
    {
        public string SYSTEM { get; set; }
        public string CLIENTS { get; set; }
        public string APPLICATION { get; set; }
        public string DEPLOYMENT { get; set; }
        public string ECOMMERCE { get; set; }
        public JObject ClientList { get; set; }
        public JObject System_info { get; set; }
        public JObject Application_info { get; set; }
        public JObject Deployments_info { get; set; }
        public JObject Ecommerce_info { get; set; }

        public JObject Logger(MemoryCache cache, string client_id)
        {
            var returnObJ = new JObject();

            returnObJ["create"] = false;
            returnObJ["update"] = false;
            returnObJ["read"] = false;
            returnObJ["delete"] = false;

            var getClientDetails = this.getClientDetails(cache, client_id);
            try
            {
                if ((bool)getClientDetails.SelectToken("logger.create"))
                {
                    returnObJ["create"] = true;
                }
            }
            catch (Exception e) { }
            try
            {
                if ((bool)getClientDetails.SelectToken("logger.update"))
                {
                    returnObJ["update"] = true;
                }
            }
            catch (Exception e) { }
            try
            {
                if ((bool)getClientDetails.SelectToken("logger.read"))
                {
                    returnObJ["read"] = true;
                }
            }
            catch (Exception e) { }
            try
            {
                if ((bool)getClientDetails.SelectToken("logger.delete"))
                {
                    returnObJ["delete"] = true;
                }
            }
            catch (Exception e) { }
            return returnObJ;
        }
        public JObject getClientDetails(MemoryCache cache, string client_id)
        {
            return (JObject)ClientCache.getClientDetails(cache, client_id + "_details", this.ClientList[client_id]["settings"].ToString() + "app-settings.json");
        }
        public JObject getClientModels(MemoryCache cache, string client_id, string model_id)
        {
            return (JObject)ClientCache.getClientDetails(cache, client_id + "_meta_data", this.ClientList[client_id]["settings"].ToString() + "client-models-kp-published.json")[model_id];
        }
        public JObject getClientModels(MemoryCache cache, string client_id)
        {
            return ClientCache.getClientDetails(cache, client_id + "_meta_data", this.ClientList[client_id]["settings"].ToString() + "client-models-kp-published.json");
        }
        public JObject getClientAdminModels(MemoryCache cache, string client_id, string model_id)
        {
            return (JObject)ClientCache.getClientDetails(cache, client_id + "_admin_meta_data", this.ClientList[client_id]["settings"].ToString() + "administration-kp-published.json")[model_id];
        }
        public JObject getClientAdminModels(MemoryCache cache, string client_id)
        {
            return ClientCache.getClientDetails(cache, client_id + "_admin_meta_data", this.ClientList[client_id]["settings"].ToString() + "administration-kp-published.json");
        }
        public void Init()
        {

            ClientList = new JObject();
            try
            {
                ClientList = JObject.Parse(this.CLIENTS);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading - Client INFO :: " + e.Message.ToString());
            }


            System_info = new JObject();
            try
            {
                System_info = JObject.Parse(this.SYSTEM);

            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading - System INFO :: " + e.Message.ToString());
            }
            Application_info = new JObject();
            try
            {
                Application_info = JObject.Parse(this.APPLICATION);

            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading - Application INFO :: " + e.Message.ToString());
            }


            Deployments_info = new JObject();
            try
            {
                Deployments_info = JObject.Parse(this.DEPLOYMENT);

            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading - Administration INFO :: " + e.Message.ToString());
            }

            Ecommerce_info = new JObject();
            try
            {
                Ecommerce_info = JObject.Parse(this.ECOMMERCE);

            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading - Ecommerce INFO :: " + e.Message.ToString());
            }
        }
        public JObject getClients()
        {
            return this.ClientList;
        }
        public JToken getClients(string id)
        {
            return this.ClientList[id];
        }

        public JObject getSystemSettings()
        {
            return this.System_info;
        }
        public JObject getApplicationSettings()
        {
            return this.Application_info;
        }
        public JObject getDeplomentsSettings()
        {
            return this.Deployments_info;
        }

        public JObject getEcommerceSettings()
        {
            return this.Ecommerce_info;
        }

        public JToken getSystemModel(string model_name)
        {
            return this.System_info.SelectToken(model_name.ToLower().Trim());
        }
        public JToken getDeploymentsModel(string model_name)
        {
            return this.Deployments_info.SelectToken(model_name.ToLower().Trim());
        }
        public JToken getEcommerceModel(string model_name)
        {
            return this.Ecommerce_info.SelectToken(model_name.ToLower().Trim());
        }
    }
}
