﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Common.Settings
{
    public class CommonSettings
    {
    }
    public class CorsInfo
    {
        public bool Enable_Cors;
        public IList<string> Allowed_Hosts;
    }
    public class ValidationErrors
    {
        public IList<string> errors { get; set; }
        public ValidationErrors()
        {
            errors = new List<string>();
        }
    }
    public static class JSONValidator
    {
        public static NJsonSchema.JsonSchema loadSchema(string schema_info)
        {
            NJsonSchema.JsonSchema schema;

            try
            {
                schema = NJsonSchema.JsonSchema.FromJsonAsync(schema_info).Result;

            }
            catch (Exception e)
            {
                throw e;
            }
            return schema;
        }
        public static ValidationErrors validateSchema(string schema_info, JToken content)
        {
            NJsonSchema.JsonSchema schema;

            try
            {
                schema = NJsonSchema.JsonSchema.FromJsonAsync(schema_info).Result;

            }
            catch (Exception e)
            {
                throw e;
            }

            return validateSchema(schema, content);
        }
        public static ValidationErrors validateSchema(NJsonSchema.JsonSchema schema, JToken content)
        {
            var list_of_errors = schema.Validate(content);

            ValidationErrors list_to_return = new ValidationErrors();
            foreach (NJsonSchema.Validation.ValidationError error in list_of_errors)
            {
                list_to_return.errors.Add(error.ToString());
            }

            return list_to_return;
        }
    }
}
