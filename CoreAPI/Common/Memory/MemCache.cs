﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Common.Memory
{
    public class MemCache
    {
        public MemoryCache Cache { get; set; }
        public MemCache()
        {
            Cache = new MemoryCache(new MemoryCacheOptions
            {
                SizeLimit = 4096,
                ExpirationScanFrequency = System.TimeSpan.FromMinutes(30)

            }); ;
        }
    }
}
