﻿using CoreAPI.Common.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Providers
{
    public class SwaggerAuthorized { 

    private readonly RequestDelegate _next;

    public SwaggerAuthorized(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context, IOptionsSnapshot<ApplicationSettings> settings)
      {

        if (context.Request.Path.StartsWithSegments("/documents")
            && !context.User.Identity.IsAuthenticated)
        {
            settings.Value.Init();
            string login_path = settings.Value.getApplicationSettings()["BASE_PATH"].ToString() + "auth/login";



            context.Response.StatusCode = StatusCodes.Status500InternalServerError;

            context.Response.ContentType = "text/html";

            await context.Response.WriteAsync("<html lang=\"en\"><body>\r\n");
            await context.Response.WriteAsync("Authendication required !<br><br>\r\n");



            await context.Response.WriteAsync("<a href=\"" + login_path + "\">Click here</a> to reirect login page<br>\r\n");
            await context.Response.WriteAsync("<script language=\"javascript\">document.location.href='" + login_path + "'</script>\r\n");
            await context.Response.WriteAsync("</body></html>\r\n");
            await context.Response.WriteAsync(new string(' ', 512));

            return;
        }

        await _next.Invoke(context);
    }
}
}
