﻿using CoreAPI.Application.Contexts;
using CoreAPI.Application.Interfaces;
using CoreAPI.Application.Repository;
using CoreAPI.Common.Settings;
using CoreAPI.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CoreAPI.Application.Data;

namespace CoreAPI.Providers
{
    public class TokenProvider
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration _config;
        private readonly TokenProviderOptions _options;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly IWebHostEnvironment _env;
        private readonly IOptions<ApplicationSettings> _settings;
        private IMongoContext _context;
        private ITimezone timeZone;
        private readonly IUnitOfWork _uow;
        string fileServerBasePath = Environment.GetEnvironmentVariable("PELLUCID_LOCAL_PATH");
        const String appFolderName = @"wwwroot/application/settings";
        readonly String folderPath;
        public TokenProvider(
            RequestDelegate next,
            IOptions<TokenProviderOptions> options,
            IOptions<ApplicationSettings> settings,
            IConfiguration config,
            IWebHostEnvironment env)
        {
            _settings = settings;
            _config = config;
            _settings.Value.Init();
            if (fileServerBasePath != null)
            {
                //Path.Combine(Directory.GetCurrentDirectory(), folderName);
                folderPath = System.IO.Path.Combine(fileServerBasePath, appFolderName);
            }
            _next = next;
            _env = env;
            _options = options.Value;

            ThrowIfInvalidOptions(_options);

            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
            _context = new MongoContext(config);
            timeZone = new TimezoneRepository(config);
            _uow = new Application.UoW.UnitOfWork(_context);
        }

        public Task Invoke(HttpContext context)
        
        {
            if (!context.Request.Path.Equals(_options.Path, StringComparison.Ordinal))
            {
                return _next(context);
            }


            if (context.Request.Method.Equals("POST") && context.Request.HasFormContentType)
            {
                var authUser = GenerateToken(context);
                if (authUser.Exception == null)
                {
                    return authUser;
                }
                else
                {
                    throw authUser.Exception;
                }
            }

            context.Response.Redirect(_options.Login);
            context.Response.StatusCode = 400;
            return context.Response.WriteAsync("Bad request.");
        }

        private async Task GenerateToken(HttpContext context)
        {
            var username = context.Request.Form["user.username"].ToString();
            var password = context.Request.Form["user.password"].ToString();
            var path = context.Request.Form["redirect"];
            var type = "-1";
            var role = "";
            var full_name = "";
            var client_id = "";
            var email = "";
            var role_details = "{}";
            var identity = await _options.IdentityResolver(username, password);
            JObject clients = new JObject();

            try
            {
                clients = JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "client_minimum_info.json"));
                if ((bool)clients[identity.FindFirst("client_id").Value.Trim().ToLower()]["active"] != true)
                {
                    if (string.IsNullOrEmpty(path))
                    {
                        var invalid = new
                        {
                            message = "Client is disabled"
                        };
                        context.Response.StatusCode = 403;
                        context.Response.ContentType = "application/json";
                        await context.Response.WriteAsync(JsonConvert.SerializeObject(invalid, _serializerSettings));
                    }
                    else
                    {
                        context.Response.StatusCode = 400;
                        context.Response.Redirect(_options.Login + "?error=1");
                        await context.Response.WriteAsync("Client is disabled.");

                    }
                    return;
                }
            }
            catch (Exception e)
            {

            }


            if (identity == null)
            {
                if (string.IsNullOrEmpty(path))
                {
                    var invalid = new
                    {
                        message = "Invalid username/password."
                    };
                    context.Response.ContentType = "application/json";
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(invalid, _serializerSettings));
                }
                else
                {
                    context.Response.StatusCode = 400;
                    context.Response.Redirect(_options.Login + "?error=1");
                    await context.Response.WriteAsync("Invalid username/password.");

                }
                return;
            }
            try
            {
                type = identity.FindFirst("user_type").Value;
                role = identity.FindFirst("user_role").Value;
                full_name = identity.FindFirst("user_full_name").Value;
                role_details = identity.FindFirst("user_role_details").Value;
                client_id = identity.FindFirst("client_id").Value.Trim().ToLower();
                email = identity.FindFirst("user_email").Value;
            }
            catch (Exception e) { }
            var now = DateTime.UtcNow;
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, username),
                new Claim(JwtRegisteredClaimNames.Sub, username),
                new Claim(JwtRegisteredClaimNames.Jti, await _options.NonceGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat,
                    new DateTimeOffset(now).ToUniversalTime().ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64),
                new Claim("user_id", username),
                new Claim("full_name", full_name),
                new Claim("user_type",  type),
                new Claim("user_role",  role),
                new Claim("client_id",  client_id),
            };
            string[] system_roles = { "system_admin", "admin", "power_user", "normal_user" };
            if (type != "-1")
            {
                if (type != "0")
                {
                    claims.Add(new Claim(ClaimTypes.Role, "base_user"));
                }

                claims.Add(new Claim(ClaimTypes.Role, "self_user"));
                try
                {

                    claims.Add(new Claim(ClaimTypes.Role, system_roles[int.Parse(type)]));
                }
                catch (Exception e)
                {
                    claims.Add(new Claim(ClaimTypes.Role, "default_user"));
                }
            }

            // Create the JWT and write it to a string
            var jwt = new JwtSecurityToken(
                issuer: _options.Issuer,
                audience: _options.Audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(_options.Expiration),
                signingCredentials: _options.SigningCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            Newtonsoft.Json.Linq.JObject defaultApplicationSettings = new Newtonsoft.Json.Linq.JObject();

            /*
            if (type == "1" || type == "0")
            {
                if(client_id == "pellucid")
                {
                    defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
                }
                else
                {
                    if (System.IO.File.Exists(_env.ContentRootPath + "//apps//" + "client_minimum_info.json"))
                    {
                        try
                        {
                            defaultApplicationSettings = (JObject) Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(clients[client_id]["settings"] + "app-settings.json"))["settings"];

                        }
                        catch (Exception e)
                        {
                            defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
                        }
                    }
                    else
                    {
                        defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
                    }
                    
                }

            }
            else
            {
                defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(role_details);
            }
            */




            if (client_id == "pellucid")
            {
                defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
            }
            else
            {
                if (System.IO.File.Exists(_env.ContentRootPath + "//apps//" + "client_minimum_info.json"))
                {
                    try
                    {
                        defaultApplicationSettings = (JObject)Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(clients[client_id]["settings"] + "app-settings.json"))["settings"];

                    }
                    catch (Exception e)
                    {
                        defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
                    }
                }
                else
                {
                    defaultApplicationSettings = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(_env.ContentRootPath + "//apps//" + "pellucid-default-apps.json"));
                }

            }

            try
            {
                await new AuditTrail(new LoggerRepository(_context, timeZone), _uow, _context).InsertLogAsync(client_id + "__" + DateTime.Now.ToString("yyyyMM"), client_id, "Authorisation", "LOGIN", username, _settings.Value.getSystemModel("pellucid_logs"), new JObject(), new { user_id = username, full_name = full_name, email = email }, context.Request.HttpContext);

            }
            catch (Exception e)
            {

            }
            var response = new
            {
                full_name = full_name,
                client_id = client_id,
                id = username,
                settings = defaultApplicationSettings,
                access_token = encodedJwt,
                role_id = role,
                type = type,
                roles = Newtonsoft.Json.Linq.JObject.Parse(role_details),
                expires_in = (int)_options.Expiration.TotalSeconds,
                datetime = new Application.Repository.TimezoneRepository(_settings.Value.Application_info.SelectToken("TIME_ZONE.COUNTRY_ZONE").ToString(), _settings.Value.Application_info.SelectToken("TIME_ZONE.DATE_FORMAT").ToString()).GetUserLocalTimeasString()
            };

            context.Session.SetString("JWToken", encodedJwt);

            // Serialize and return the response
            if (string.IsNullOrEmpty(path))
            {
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(JsonConvert.SerializeObject(response, _serializerSettings));
            }
            else
            {
                context.Response.Redirect(path);
            }

            //context.Response.ContentType = "application/json";
            //await context.Response.WriteAsync(JsonConvert.SerializeObject(response, _serializerSettings));
        }

        private static void ThrowIfInvalidOptions(TokenProviderOptions options)
        {
            if (string.IsNullOrEmpty(options.Path))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.Path));
            }

            if (string.IsNullOrEmpty(options.Issuer))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.Issuer));
            }

            if (string.IsNullOrEmpty(options.Audience))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.Audience));
            }

            if (options.Expiration == TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(TokenProviderOptions.Expiration));
            }

            if (options.IdentityResolver == null)
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.IdentityResolver));
            }

            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.SigningCredentials));
            }

            if (options.NonceGenerator == null)
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.NonceGenerator));
            }
        }
    }
}
